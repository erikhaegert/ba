import numpy as np
import pandas as pd
import torch
import matplotlib.pyplot as plt
import seaborn as sns
from torch.utils.data import DataLoader, SubsetRandomSampler
from data_handler import  GroundingStateDataSet as GSData
from sequence_padder import PadSequence
from grounding_state_hcn import GroundingStateHCN
import torch.nn.functional as F
from sklearn.metrics import classification_report, accuracy_score, confusion_matrix, f1_score

from collections import Counter
from k_fold_crossvalidation import KFold

class Trainer():
	""""Trainer class. Trains model"""
	def __init__(self, data_path, batch_size = 32, validation_split=0.3, shuffle_dataset = True, random_seed=50, epochs = 50, verbose = True, test = True, save = False, loss_chart = True, cross_validate=False):
		self.data = GSData(data_path)
		self.batch_size = batch_size
		self.validation_split = validation_split
		self.shuffle_dataset = shuffle_dataset
		self.random_seed= random_seed
		self.epochs = epochs
		self.cat_dims = self.data[0][1].shape[-1]
		self.dims = self.data[0][0].shape[-1]
		self.out_dim = self.data[0][2].shape[-1]
		self.train_set, self.val_set = self.split_data()
		self.model = GroundingStateHCN(self.dims, self.cat_dims, self.out_dim, self.batch_size).float()#, masks = masks, inv_map = inv_map, hidden_size= 128)
		self.optimizer = torch.optim.Adadelta(
			filter(lambda p: p.requires_grad, self.model.parameters()),lr=0.2, weight_decay=0.05)
		self.criterion = torch.nn.CrossEntropyLoss(ignore_index=-1, reduction="mean")#, weight=torch.Tensor([1,1,1,1,1,0.1,1]))
		self.verbose = verbose
		self.test = test
		self.save = save
		self.loss_chart = loss_chart
		self.cross_validate = cross_validate

	# collate function for batch generation
	def custom_collate(self,batch):
		c = PadSequence()
		return c(batch)

	#generates and splits data into train and dev sets of padded batches
	def split_data(self):
		dataset_size = len(self.data)
		indices = list(range(dataset_size))
		split = int(np.floor(self.validation_split * dataset_size))
		if self.shuffle_dataset:
			np.random.seed(self.random_seed)
			np.random.shuffle(indices)
		train_indices, val_indices = indices[split:], indices[:split]
		train_sampler = SubsetRandomSampler(train_indices)
		valid_sampler = SubsetRandomSampler(val_indices)
		train_loader = DataLoader(self.data,
									batch_size=self.batch_size, 
									sampler=train_sampler, 
									collate_fn=self.custom_collate)
		validation_loader = DataLoader(self.data,
										batch_size=self.batch_size,
										sampler=valid_sampler, 
										collate_fn=self.custom_collate)
		return train_loader, validation_loader

	def cross_validation(self,k):
		self.cross_validate=True
		self.loss_chart = False
		data_set_size= len(self.data)
		cv = KFold(k,data_set_size,self.shuffle_dataset,self.random_seed)
		f1_scores = []
		for train_indices, val_indices in cv.get_splits():
			train_sampler = SubsetRandomSampler(train_indices)
			valid_sampler = SubsetRandomSampler(val_indices)
			self.train_set = DataLoader(self.data,
									batch_size=self.batch_size, 
									sampler=train_sampler, 
									collate_fn=self.custom_collate)
			self.val_set = DataLoader(self.data,
										batch_size=self.batch_size,
										sampler=valid_sampler, 
										collate_fn=self.custom_collate)
			if torch.cuda.is_available():
				self.model = GroundingStateHCN(self.dims, self.cat_dims, self.out_dim, self.batch_size).float().cuda()
        
			else:
				self.model = GroundingStateHCN(self.dims, self.cat_dims, self.out_dim, self.batch_size).float()
			self.optimizer = torch.optim.Adadelta(
			filter(lambda p: p.requires_grad, self.model.parameters()),lr=0.2, weight_decay=0.05)
			self.criterion = torch.nn.CrossEntropyLoss(ignore_index=-1, reduction="mean")
			f1_scores.append(self.train())
		print("cross validated weighted average f1 score: {}".format(float(sum(f1_scores))/float(k)))
		return 


	#training loop 
	def train(self):
		loss_epoch = []
		f1_scores = []
		for j in range(self.epochs):
			self.model.train()		
			for batch_index, (features, cats, labels, masks, inv_map, lengths) in enumerate(self.train_set):
				losses = []
				self.model.hidden = self.model.init_hidden(features.shape[0])
				self.optimizer.zero_grad()
				res = self.model(features, cats, lengths, masks)
				label_size = self.model.label_size
				#reshape logits for loss function
				logits = self.model.logits.view(self.model.logits.shape[0]*self.model.logits.shape[1],label_size)
				#logits = self.model.probs.view(self.model.probs.shape[0]*self.model.probs.shape[1],self.model.label_size)

				minmap = inv_map-1
				if torch.cuda.is_available():
					loss = self.criterion(logits, minmap.view(-1).cuda())
				else:
					loss = self.criterion(logits, minmap.view(-1))
				loss.backward()
				self.optimizer.step()
				#if self.verbose:
				#	print(loss)
				losses.append(loss.item())
				#if self.loss_chart:
				#	losses.append(loss.item())
			
			print('\n\n:: {}.tr loss {}'.format(j+1, float(sum(losses))/float(len(losses))))
			loss_epoch.append(float(sum(losses))/float(len(losses)))
			f1, dev_loss = self.evaluate()
			print(':: {}.dev loss {}'.format(j+1, float(sum(dev_loss))/float(len(dev_loss))))
			f1_scores.append(f1)
			print(':: {}.dev weighted f1: {}\n'.format(j+1, f1))
				
		if self.save:
			from datetime import datetime
			tim = datetime.now()
			date = "".join(str(tim).split())
			date = date.replace(":", "-")
			with open("../chkpt/hcn_model" + date + ".p", "wb") as f:
				torch.save(net, f)
		if self.loss_chart:
			print(dev_loss)
			print(loss_epoch)
			plt.plot(dev_loss)
			plt.plot(loss_epoch[::len(self.train_set)])
			plt.plot(accs[::len(self.train_set)])
			plt.title('loss')
			plt.ylabel('loss')
			plt.xlabel('epoch')
			plt.legend(['tr_loss', "dev_loss",'acc'], loc='upper left')
			plt.show()

		if self.cross_validate:
			return max(f1_scores)

	def evaluate(self):
		actual_y =[]
		actual_res = []
		dev_loss = []
		with torch.no_grad():
			self.model.eval()
			for (features, cats, labels, masks, inv_map, lengths) in self.val_set:	
				inv_map = inv_map
				res = self.model(features,cats, lengths, masks)
				minmap = inv_map-1
				logits = self.model.logits
				logits = self.model.logits.view(self.model.logits.shape[0]*self.model.logits.shape[1],self.model.label_size)
				#logits = self.model.probs.view(self.model.probs.shape[0]*self.model.probs.shape[1],self.model.label_size)

				if torch.cuda.is_available():
					loss = self.criterion(logits, minmap.view(-1).cuda())
				else:
					loss = self.criterion(logits, minmap.view(-1))
				dev_loss.append(loss.item())
				y = inv_map.detach().numpy().flatten().tolist()
				res = res.detach().cpu().numpy().flatten().tolist()
				for i in range(len(y)):
					if y[i]!=0:
						actual_y.append(y[i])
						actual_res.append(res[i])
		#print(actual_y)
		#print(actual_res)

		print("CLASSREPORT")
		print(classification_report(actual_y,actual_res, labels = [1,2,3,4,5,6,7]))
		#print("ACC: ", accuracy_score(actual_y,actual_res))
		print(confusion_matrix(actual_y, actual_res))
		return f1_score(actual_y, actual_res, average="weighted"), dev_loss



if __name__ == "__main__":
	trainer = Trainer("../data/latest_annotation.p")
	trainer.cross_validation(5)