import torch

class PadSequence:
	"""Class padds batches."""
	def __call__(self, batch):
		sorted_batch = sorted(batch, key=lambda x: x[0].shape[0], reverse=True)
		sequences = [x[0] for x in sorted_batch]
		sequences_padded = torch.nn.utils.rnn.pad_sequence(sequences, batch_first=True)
		cats_list = [x[1] for x in sorted_batch]
		padded_cats = torch.nn.utils.rnn.pad_sequence(cats_list, batch_first=True)
		cats = torch.FloatTensor(padded_cats.float())
		onehot_list =  [x[2] for x in sorted_batch]
		padded_onehot = torch.nn.utils.rnn.pad_sequence(onehot_list, batch_first=True)
		onehots = torch.LongTensor(padded_onehot)
		mask_list = sequences = [x[3] for x in sorted_batch]
		padded_masks = torch.nn.utils.rnn.pad_sequence(mask_list, batch_first=True)
		masks = torch.DoubleTensor(padded_masks)
		label_list = sequences = [x[4] for x in sorted_batch]
		padded_labels = torch.nn.utils.rnn.pad_sequence(label_list, batch_first=True)
		labels = torch.LongTensor(padded_labels)
		lengths = [x[5] for x in sorted_batch]
		return sequences_padded, cats, onehots, masks, labels, lengths
