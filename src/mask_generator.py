import numpy as np
import pandas as pd 

class ActionMask():
	"""
	A class implementing the actionmask.
	Attributes:
	-----------
		numpy.array: mask
		the action mask

	"""

	def __init__(self, include_next_init = True):
		self.next_init = include_next_init
		if self.next_init:
			self.states =  [0,1,2,3,4,5,6,7] # 5=F, 6=C, 7=N(ew DU)
		else:
			self.states =  [0,1,2,3,4,5,6] # 5=F, 6=C
		self.grounding_acts = ["InitI", "ContI", "ContR","RepI", "RepR", "ReqRepI", "ReqRepR", "AckI", "AckR", "ReqAckI", "ReqAckR","CanI", "CanR", "NewI", "NewR"]
		self.automaton = self.build_automaton()
		self.mask_map= self.build_mask_map()
		return

	def __str__(self):
		return 

	def __call__(self, role, from_state):
		if type(from_state) != int:
			from_state = int(from_state)
		try:
			return self.mask_map[role][from_state]
		except: 
			if role not in [0,1]:
				raise TypeError("Role {} not known! Should be either 1 (Initiator) or 0 (Recipient)".format())
			elif from_state not in self.states:
				raise TypeError("State {} not in Automaton. Must be in {}".format(from_state, self.states))

		


	#original automaton including state 4
	def build_automaton(self):
		"""
		Defines the DU-Automaton.

		Returns
		--------
			None
		"""
		d = {num:pd.Series(np.array([-1 for i in range(len(self.grounding_acts))]), dtype=int, index = self.grounding_acts) for num in range(len(self.states))}
		self.automaton = pd.DataFrame(d)
		self.add_transition(0,1,"InitI")
		self.add_transition(1,1,["ContI","RepI", "ReqAckI"])
		self.add_transition(4,4,["ContI", "RepI", "ReqRepI"])
		self.add_transition(2,2,["ContR", "RepR", "ReqRepR"])
		self.add_transition(3,3,["ContR", "RepR", "ReqAckR"])	
		self.add_transition(2,1,["RepI"])
		self.add_transition(3,1,["RepI"])
		self.add_transition(5,1,["RepI", "ReqAckI"])
		self.add_transition(1,3,"RepR")
		self.add_transition(4,3,"RepR")
		self.add_transition(5,3,["RepR", "ReqAckR"])
		self.add_transition(2,4,"ReqRepI")
		self.add_transition(3,4,"ReqRepI")
		self.add_transition(5,4,"ReqRepI")
		self.add_transition(1,2,"ReqRepR")
		self.add_transition(3,2,"ReqRepR")
		self.add_transition(4,2,"ReqRepR")
		self.add_transition(5,2,"ReqRepR")
		self.add_transition(3,5,"AckI")
		self.add_transition(4,1,"AckI")
		self.add_transition(5,5,["AckI", "AckR"])
		self.add_transition(1,5,"AckR")
		self.add_transition(2,5,"AckR")
		self.add_transition(1,6,["CanI","CanR"])
		self.add_transition(2,6,["CanI","CanR"])
		self.add_transition(3,6,["CanI","CanR"])
		self.add_transition(4,6,["CanI","CanR"])
		self.add_transition(5,6,["CanI","CanR"])
		if self.next_init:
			self.add_transition(1,7,["NewI","NewR"])
			self.add_transition(2,7,["NewI","NewR"])
			self.add_transition(3,7,["NewI","NewR"])
			self.add_transition(4,7,["NewI","NewR"])
			self.add_transition(5,7,["NewI","NewR"])
			self.add_transition(6,7,["NewI","NewR"])
		return self.automaton



	def build_automaton_(self):
		"""
		Defines the DU-Automaton.

		Returns
		--------
			None
		"""
		d = {num:pd.Series(np.array([-1 for i in range(len(self.grounding_acts))]), dtype=int, index = self.grounding_acts) for num in range(len(self.states))}
		self.automaton = pd.DataFrame(d)
		self.add_transition(0,1,"InitI")
		self.add_transition(1,1,["ContI","RepI", "ReqAckI"])
		#self.add_transition(4,4,["ContI", "RepI", "ReqRepI"])
		self.add_transition(2,2,["ContR", "RepR", "ReqRepR"])
		self.add_transition(3,3,["ContR", "RepR", "ReqAckR"])	
		self.add_transition(2,1,["RepI"])
		self.add_transition(3,1,["RepI"])
		self.add_transition(4,1,["RepI", "ReqAckI"])
		self.add_transition(1,3,"RepR")
		#self.add_transition(4,3,"RepR")
		self.add_transition(4,3,["RepR", "ReqAckR"])
		#self.add_transition(2,4,"ReqRepI")
		#self.add_transition(3,4,"ReqRepI")
		#self.add_transition(5,4,"ReqRepI")
		self.add_transition(1,2,"ReqRepR")
		self.add_transition(3,2,"ReqRepR")
		#self.add_transition(4,2,"ReqRepR")
		self.add_transition(4,2,"ReqRepR")
		self.add_transition(3,4,"AckI")
		#self.add_transition(4,1,"AckI")
		self.add_transition(4,4,["AckI", "AckR"])
		self.add_transition(1,4,"AckR")
		self.add_transition(2,4,"AckR")
		self.add_transition(1,5,["CanI","CanR"])
		self.add_transition(2,5,["CanI","CanR"])
		self.add_transition(3,5,["CanI","CanR"])
		#self.add_transition(4,6,["CanI","CanR"])
		self.add_transition(4,5,["CanI","CanR"])
		if self.next_init:
			self.add_transition(1,6,["NewI","NewR"])
			self.add_transition(2,6,["NewI","NewR"])
			self.add_transition(3,6,["NewI","NewR"])
			#self.add_transition(4,6,["NewI","NewR"])
			self.add_transition(4,6,["NewI","NewR"])
			self.add_transition(5,6,["NewI","NewR"])
		return self.automaton
		
	def add_transition(self,from_state, to_state, g_act):
		"""
			Adds transition to DU-automaton. 

			Parameters
			----------
				from_state: int
					state label (outgoing)
				to_state: int
					state_label (ingoing)
				g_act: str
					grounding act

			Returns
			-------
				None
			"""
		if type(g_act)== list():
			for ga in g_act:
				self.automaton[from_state][ga]=to_state
			return
		self.automaton[from_state][g_act]=to_state
		return 


	def build_mask_map(self):
		# 0 = Initiator
		# 1 = Recipient
		mask_map = {0: {state: np.zeros(len(self.states), dtype=float) for state in self.states},\
					1: {state: np.zeros(len(self.states), dtype=float) for state in self.states}}
		transformed = self.automaton.T
		for i, row in transformed.iterrows():
			for index, val in row.iteritems():
				if val > 0:
					if index.endswith("I"):
						mask_map[0][i][val]=1.0
					else:
						mask_map[1][i][val]=1.0
		return mask_map

if __name__ == "__main__":
	import json
	m = ActionMask()
	print(m(0,4))
	print(m(1,4))
	