import numpy as np

class KFold():
	def __init__(self, k, data_size, shuffle=True, seed = 50):
		self.k = k
		self.data_size = data_size
		self.indices= list(range(data_size))
		if shuffle:
			np.random.seed(seed)
			np.random.shuffle(self.indices)
		return 


	def get_test_indices(self):
		fold_size = int(len(self.indices)/self.k)
		curr=0
		for k in range(self.k):
			start = curr
			end = start + fold_size
			curr = end
			yield self.indices[int(start):int(end)]



	def get_splits(self):
		for test in self.get_test_indices():
			training = np.setdiff1d(self.indices, test)
			yield training, test

			

