from flair.embeddings import (
	WordEmbeddings,
	FlairEmbeddings,
	DocumentPoolEmbeddings,
	StackedEmbeddings,
)
from flair.data import Sentence
import numpy as np
import torch
################
#Implemented by Santi Pornavalai
#
###############
class Embedder:
	"""Embedder class. """
	def __init__(self, np_vectors=True, use_flair = True):	   
		self.np_vectors = np_vectors
		self.use_flair = use_flair
		self.flair_embeddings_list = ["de-forward", "de-backward"]
		#self.flair_embeddings_list = ['de-crawl']
		self.german_embedding = WordEmbeddings('de')
		self.embedding_model = []
		self.build_stacked_documents()
		self.dim = self.embedding_model.embedding_length
		
	
	def build_stacked_documents(self):
		#  build average document embeddings
		if self.use_flair == True:
			embeddings = [
				FlairEmbeddings(emb_name) for emb_name in self.flair_embeddings_list
			]
			embeddings.append(WordEmbeddings('de'))
		else:
			embeddings =  [WordEmbeddings('de')]

		self.embedding_model = DocumentPoolEmbeddings(embeddings, pooling="mean")

	def embed(self, utterance):
		# embeds the whole document by averaging word embeddings
		
		sent = Sentence(utterance)
		self.embedding_model.embed(sent)
		if self.np_vectors:
			return sent.get_embedding().detach().numpy()
		return sent.get_embedding()


if __name__ == "__main__":
	e = Embedder()
	first = e.embed("und dann")
	second = e.embed("und dann legst du das hin")
	print(first)
	print(second)