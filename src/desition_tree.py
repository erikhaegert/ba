import pandas as pd 
import pickle as pkl 
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier, RandomForestRegressor
from sklearn.metrics import accuracy_score, f1_score
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import GridSearchCV
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import cross_val_score

feature_headers = ['is_same_speaker',\
	   'is_repetition', 'is_addition', 'prev_state', 'role',\
	   'is_complete', 'is_declarative', 'is_imperative', 'yes_no', 'is_wh',\
	   'is_alternative', 'starts_with_conn', 'is_explicit_req_ack',\
	   'present_for_check', 'is_neg_aff', 'is_eval', 'is_backchannel',\
	   'spkr1', 'spkr2']



def split_dataset(dataset, train_percentage, feature_headers, target_header):
	"""
	Split the dataset with train_percentage
	:param dataset:
	:param train_percentage:
	:param feature_headers:
	:param target_header:
	:return: train_x, test_x, train_y, test_y
	"""

	# Split dataset into train and test dataset
	train_x, test_x, train_y, test_y = train_test_split(dataset[feature_headers], dataset[target_header],
														train_size=train_percentage)
	print(train_x.shape)												
	return train_x, test_x, train_y, test_y

def random_forest_classifier(features, target):
	"""
	To train the random forest classifier with features and target data
	:param features:
	:param target:
	:return: trained random forest classifier
	"""
	
	#scaler = StandardScaler()
	#features = scaler.fit_transform(features)
	#target = scaler.transform(target)
	clf = RandomForestClassifier()
	clf.fit(features, target)
	return clf

def evaluate(model, test_features, test_labels):
	predictions = model.predict(test_features)
	errors = abs(predictions - test_labels)
	mape = 100 * np.mean(errors / test_labels)
	accuracy = 100 - mape
	print('Model Performance')
	print('Average Error: {:0.4f} degrees.'.format(np.mean(errors)))
	print('Accuracy = {:0.2f}%.'.format(accuracy))
	
	return accuracy

def grid_search(train_x, train_y, test_x, test_y):
	param_grid = {
		'bootstrap': [True],
		'max_depth': [80, 90, 100, 110],
		'max_features': [2, 3],
		'min_samples_leaf': [3, 4, 5],
		'min_samples_split': [8, 10, 12],
		'n_estimators': [100, 200, 300, 1000]
	}
	# Create a based model
	rf = RandomForestClassifier()
	# Instantiate the grid search model
	grid_search = GridSearchCV(estimator = rf, param_grid = param_grid, 
							cv = 3, n_jobs = -1, verbose = 2)
	grid_search.fit(train_x, train_y)
	grid_search.best_params_
	best_grid = grid_search.best_estimator_
	predictions = best_grid.predict(test_x)
	print("Train Accuracy :: ", accuracy_score(train_y, best_grid.predict(train_x)))
	print("Test Accuracy  :: ", accuracy_score(test_y, predictions))
	
	return best_grid

def main():
	data = df = pkl.load(open("../data/latest_annotation.p","rb"))
	train_x, test_x, train_y, test_y = split_dataset(data, 0.7, feature_headers, "state")
	scaler = StandardScaler()
	train_x = scaler.fit_transform(train_x)
	test_x = scaler.transform(test_x)
	trained_model = random_forest_classifier(train_x, train_y)
	print("Trained model :: ", trained_model)
	predictions = trained_model.predict(test_x)
	print("WF1::", f1_score(test_y, predictions, average="weighted"))
	scores = cross_val_score(trained_model, data[feature_headers], data["state"], cv=5)
	print("Cross val Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))

	# Train and Test Accuracy
	print("Train Accuracy :: ", accuracy_score(train_y, trained_model.predict(train_x)))
	print("Test Accuracy  :: ", accuracy_score(test_y, predictions))

	best_grid = grid_search(train_x,train_y, test_x, test_y)

	scores = cross_val_score(best_grid, data[feature_headers], data["state"], cv=5)
	print("Cross val Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
	return 

if __name__ == "__main__":
	main()