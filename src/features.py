import sys 
import spacy 
from flair.models import SequenceTagger
from flair.data import Sentence
import re
import pickle as pkl
#import utils 
from nltk.corpus import stopwords
class Features():
	"""Class for feature extraction"""
	def __init__(self,spacy_model = 'de_core_news_md'):
			self.tagger = spacy.load(spacy_model)
			self.stopwords = set(["ok", "okay","mhm","hm","aha", "achso", "ja", "nein", "nee","so", "also", "und","dann"])
			return 



	def is_declarative(self, sentence):
		tagged_sentence  = self.tagger(sentence)
		verb_occ = False
		wh_occ = False
		for i in range(len(tagged_sentence)):
			#remember wh-occurence
			if tagged_sentence[i].tag_ in ["PWS", "PWAV", "PWAT"]:
				wh_occ =True
			#remeber embedded finite verb occurence
			elif i > 0 and tagged_sentence[i].tag_[:2] in ["VV","VA","VM"] and tagged_sentence[i].dep_!= "ROOT":
				verb_occ = True
			elif i > 0 and tagged_sentence[i].tag_ in ["VVFIN", "VAFIN", "VMFIN"] and tagged_sentence[i].dep_== "ROOT":
					#print("wh occ: ", wh_occ)
					#print("fin occ: ", verb_occ)
					if wh_occ == True and verb_occ == True:
						return 1
					if wh_occ == True and verb_occ == False:
						return 0
					if i == 1 and tagged_sentence[0].dep_ == "ju":
						return 0
					postv_wh =False
					embv = False
					perspron = False
					for j in range(i, len(tagged_sentence)):
						#is there a wh-word following the root verb
						if tagged_sentence[j].tag_ in ["PWS", "PWAV", "PWAT", "PIS"]:
							postv_wh = True
						#
						elif tagged_sentence[j].tag_[:2] in ["VV","VA","VM"]:
							embv = True

						elif tagged_sentence[j].tag_ == "PPER":
							perspron =True
					#print("postwh occ: ", postv_wh)
					#print("pstv occ: ", embv)
					#print("pron", perspron)
					if postv_wh == True and embv == True and perspron == True:
						
						return 1
					elif postv_wh == False:
						return 1
		return 0

	def asserts_content_as_shared(self,sentence):
		tagged_sentence = self.tagger(sentence)
		rootv = 0
		for i in range(1,len(tagged_sentence)):
			if tagged_sentence[i].dep_=="ROOT":
				rootv = i
			elif tagged_sentence[i].text in ["ja", "doch"] and rootv < i:
				return 1
		return 0

	def is_imperative(self,sentence):
		tagged_sentence  = self.tagger(sentence)
		for token in tagged_sentence:
			if token.tag_ in "VVIMP" and token.dep_== "ROOT":
				return 1
		return 0

	def is_yes_no(self, sentence):
		intro = r"^ ?ok |^ ?mhm |^ ?ja |^ ?so |^ ?na "
		if re.match(intro, sentence):
			sentence = re.sub(intro, "", sentence)
		tagged_sentence = self.tagger(sentence)
		if len(tagged_sentence) > 1:	
			first, second = tagged_sentence[0], tagged_sentence[1]
			if (first.tag_ in  ["VVFIN", "VAFIN", "VMFIN"] or ((first.tag_ == "KON" or first.dep_ == "ju") and second.tag_ in  ["VVFIN", "VAFIN", "VMFIN"])):
				return 1
		elif self.is_tag_q(sentence) == 1:
			return 1
		return 0
	#Verbessern: du willst was kaufen 
	def is_wh(self,sentence):
		if self.is_declarative(sentence) == 1 or self.is_tag_q(sentence)==1:
			return 0
		tagged_sentence = self.tagger(sentence)
		for t in tagged_sentence:
			if t.tag_ in ["PWS", "PWAV", "PWAT"]:
				return 1
		return 0

	def is_complete(self, sentence):
		tagged_sentence = self.tagger(sentence)
		sb = False
		fin = False
		imp = False
		for token in tagged_sentence:
			if token.tag_.endswith("FIN") and token.dep_== "ROOT":
				fin = True
			elif token.tag_.endswith("IMP") and token.dep_== "ROOT":
				imp=True
			elif token.dep_ == "sb":
				sb =True
		if sb == True and fin == True:
			return 1
		elif imp == True:
			return 1
		return 0
	
	def is_tag_q(self, sentence):
		if " oder " in sentence:
			ssplit = sentence.split()
			print("SPLIT: ", ssplit)
			for i in range(len(ssplit)):
				if (ssplit[i].strip() == "oder" and ssplit[i+1].strip() in ["doch","etwa", "nicht","wie", "was", "wo", "wohin"])\
					or (i == len(ssplit)-1 and  ssplit[i].strip() == "oder" and self.is_complete(sentence) and self.is_declarative(sentence)) :
					return 1
		return 0

	def is_alternative(self, sentence):
		if " oder " in sentence:
			if self.is_complete(sentence) and self.is_declarative(sentence):
				return 0
			ssplit = sentence.split()
			print("SPLIT: ", ssplit)
			for i in range(1,len(ssplit)):
				if ssplit[i].strip() == "oder" and ssplit[i+1].strip() not in ["doch","etwa", "nicht","wie", "was", "wo", "wohin"]:
					return 1
		return 0

	def present_for_check(self, sentence):
		if sentence.startswith("also"):
			return 1
		return 0

	def is_explitcit_request_for_ack(self, sentence):
		if sentence.endswith(" ne")\
			or sentence.endswith("stimmts")\
			or sentence.endswith("stimmt das"):
			return 1
		return 0

	def starts_with_conn(self, sentence):
		tagged_sentence = self.tagger(sentence)
		if len(tagged_sentence) > 0:
			first = tagged_sentence[0]
			if first.tag_ == "KON":
				return 1
		if sentence.startswith("so ") or sentence.startswith("jetzt ") or sentence.startswith("dann "):
			return 1
		return 0

	def is_same_speaker(self,prev_spkr, curr_spkr):
		if prev_spkr == curr_spkr:
			return 1
		return 0

	def is_same_role(self, prev_act, curr_act):
		p = prev_act[-1]
		c = curr_act[-1]
		if p == c:
			return 1
		return 0

	def is_neg_or_aff(self, sentence):
		if sentence.startswith("ja") \
			or sentence.startswith("ja ")\
			or sentence.startswith("nein")\
			or sentence.startswith("nö") \
			or sentence.startswith("jup") \
			or sentence.startswith("nee"):
			return 1
		return 0

	def is_evaluation(self, sentence):	
		if sentence.startswith("stimmt") \
			or sentence.startswith("genau") \
			or sentence.startswith("korrekt") \
			or sentence.startswith("super") \
			or sentence.startswith("gut") \
			or sentence.startswith("richtig"):
			return 1
		return 0
	
	def is_backchannel(self, sentence):
		if sentence.startswith("ok") \
			or sentence.startswith("okay") \
			or sentence.startswith("verstehe ") \
			or sentence.startswith("mhm") \
			or sentence.startswith("oki doki") \
			or sentence.startswith("ach so") \
			or sentence.startswith("aha") \
			or sentence.startswith("achso"):
			return 1
		return 0


	def is_repetition(self, uttdeque):
		if len(uttdeque)==1:
			return 0
		current_utt = uttdeque[0]
		if len(current_utt.strip())==1:
			return 0
		current_utt = current_utt.strip().split()
		prevutts = list(uttdeque[slice(1,len(uttdeque))])
		if current_utt[0] == prevutts[0]:
			return 1
		elif self.all_stopw(current_utt):
			return 0
		for i, utt in enumerate(prevutts):
			utt = utt.strip().split()
			if self.is_sublist(current_utt, utt):
				print("REP: ", i, utt, "CURR: ", current_utt)
				return 1
		return 0

		
	def is_addition(self,uttdeque):
		if len(uttdeque) == 1:
			return 0
		current_utt = uttdeque[0]
		prevutts = list(uttdeque[slice(1,len(uttdeque))])
		current_utt = current_utt.strip().split()
		for i, utt in enumerate(prevutts):	
			if len(utt.strip())==1:
				return 0
			utt = utt.strip().split()
			if self.all_stopw(utt):
				return 0
			elif self.is_sublist(utt, current_utt):
				#print("ADD: ", i, utt, "CURR: ", current_utt)
				return 1
		return 0

	def all_stopw(self, l1):
		for e in l1:
			if e not in self.stopwords:
				return False
		return True

	def is_sublist(self, l1, l2):
		if len(l1)>len(l2):
			return False
		for i in range(0, len(l2)-len(l1)+1):
			if l1 == l2[i:i+len(l1)]:
				return True
		return False

		return ls1 == ls2	

	def pprint(self, sent, tagged):
		#stagged = self.tagger(sent)
		print("TEXT: {}\n\
			IS DECLARATIVE: {}\n\
			IS YES NO: {}\n\
			IS WH: {}\n\
			STARTS KONN: {}\n\
			IS NEG: {}\n\
			IS BACKCHANNEL: {}\n\
			IS COMPLETE: ".format(sent,\
									self.is_yes_no(tagged),\
									self.is_declarative(tagged),\
									self.is_wh(tagged),\
									self.starts_with_conn(tagged),\
									self.is_neg_or_aff(sent),\
									self.is_backchannel(sent), \
									self.is_complete(tagged)))





if __name__ == "__main__":
	f = Features()
	#s = "und jetzt schraubst du diesen reifen von rechts oder von links an den roten würfel"
	while True:
		s = input("sentence: ")
		tagged = f.tagger(s)
		for t in tagged:
			print(t, t.tag_ , t.dep_)
		print("dec:", f.is_declarative(s), "compl: ", f.is_complete(s), "yes no:", f.is_yes_no(s), "wh: ", f.is_wh(s), "tag: ", f.is_tag_q(s) ," alt: ", f.is_alternative(s))
########


"""with open(sys.argv[1]) as infile:
	prevsent = "<START>"
	for line in infile:
		sent = line.split("\t")[0]
		tagged = fs.tagger(sent)
		print("TEXT: {}\n\
			IS YES NO: {}\n\
			IS DECLARATIVE: {}\n\
			IS WH: {}\n\
			STARTS KONN: {}\n\
			IS NEG: {}\n\
			IS BACKCHANNEL: {}\n\
			IS COMPLETE: {}\n\
			IS REPETITION: {}\n\
			IS ADDITION: {}\n".format(sent,\
									fs.is_yes_no(tagged),\
									fs.is_declarative(tagged),\
									fs.is_wh(tagged),\
									fs.starts_with_conn(tagged),\
									fs.is_neg_or_aff(sent),\
									fs.is_backchannel(sent), \
									fs.is_complete(tagged),\
									fs.is_repetition(prevsent, sent),\
									fs.is_addition(prevsent, sent)))

		prevsent = sent"""
"""from collections import Counter
import json
confusion = {}
fs = Features()
with open(sys.argv[1]) as infile:
	prevsent = "<START>"
	prev_spkr = "first"
	prev_act = "0"
	c = 1
	for line in infile:
		if line.startswith("#"):
			continue
		if len(line.split("\t"))!= 4:
			print(c, line)
			c+=1
			continue
		sent, spkr, state, act = line.split("\t")[0].strip(),line.split("\t")[1].strip(), line.split("\t")[2].strip(), line.split("\t")[3].strip()
		tagged = fs.tagger(sent)
		sent = re.sub(r" hm |^hm | äh |^äh ", "") or sent)
		if len(sent)== 0:
			continue
		if confusion.get(act,0) == 0:
			confusion[act]= Counter()
		confusion[act]["total"]+=1
		confusion[act]["yes/no"] += int(fs.is_yes_no(tagged))
		confusion[act]["declarative"] += int(fs.is_declarative(tagged))
		confusion[act]["is_wh"] += int(fs.is_wh(tagged))
		confusion[act]["starts_with_conn"] += int(fs.starts_with_conn(tagged))	
		confusion[act]["neg/aff"] += int(fs.is_neg_or_aff(sent))
		confusion[act]["backchannel"] += int(fs.is_backchannel(sent))
		confusion[act]["is_complete"] += int(fs.is_complete(tagged))
		confusion[act]["is_repetition"] += int(fs.is_repetition(prevsent, sent))
		confusion[act]["is_addition"] += int(fs.is_addition(prevsent, sent))
		confusion[act]["is_same_speaker"] += int(fs.is_same_speaker(prev_spkr, spkr))
		confusion[act]["is_same_role"] += int(fs.is_same_role(prev_act, act))
		prevsent = sent
		prev_spkr = spkr
		prev_act = act
		c+=1
	#for key in confusion.keys():
	#	total = confusion[key]["total"]
	#	for act in confusion[key].keys():
	#		if act == "total":
	#			continue	
	#		confusion[key][act] = round(float(confusion[key][act])/float(total),2)
	print(confusion)
	print(json.dumps(confusion, indent=4, sort_keys=True))
	pkl.dump(confusion, open(sys.argv[2],"wb"))
	with open(sys.argv[1]) as infile:
		prevsent = "<START>"
		prev_spkr = "first"
		prev_act = "0"
		c = 1
		confusion = {}
		for line in infile:
			if line.startswith("#"):
				continue
			if len(line.split("\t"))!= 4:
				print(c, line)
				c+=1
				continue
			sent, spkr, state, act = line.split("\t")[0].strip(),line.split("\t")[1].strip(), line.split("\t")[2].strip(), line.split("\t")[3].strip()
			tagged = fs.tagger(sent)
			sent = re.sub(r" hm |^hm | äh |^äh ", "") or sent)
			if len(sent)== 0:
				continue
			if confusion.get(state,0) == 0:
				confusion[state]= Counter()
			confusion[state]["total"]+=1
			confusion[state]["yes/no"] += int(fs.is_yes_no(tagged))
			confusion[state]["declarative"] += int(fs.is_declarative(tagged))
			confusion[state]["is_wh"] += int(fs.is_wh(tagged))
			confusion[state]["starts_with_conn"] += int(fs.starts_with_conn(tagged))	
			confusion[state]["neg/aff"] += int(fs.is_neg_or_aff(sent))
			confusion[state]["backchannel"] += int(fs.is_backchannel(sent))
			confusion[state]["is_complete"] += int(fs.is_complete(tagged))
			confusion[state]["is_repetition"] += int(fs.is_repetition(prevsent, sent))
			confusion[state]["is_addition"] += int(fs.is_addition(prevsent, sent))
			confusion[state]["is_same_speaker"] += int(fs.is_same_speaker(prev_spkr, spkr))
			confusion[state]["is_same_role"] += int(fs.is_same_role(prev_act, act))
			prevsent = sent
			prev_spkr = spkr
			prev_act = act
			c+=1
		print(confusion)
		#for key in confusion.keys():
		#	total = confusion[key]["total"]
		#	for act in confusion[key].keys():
		#		if act == "total":
		#			continue	
		#		confusion[key][act] = round(float(confusion[key][act])/float(total),2)
		print(json.dumps(confusion, indent=4, sort_keys=True))
		pkl.dump(confusion, open(sys.argv[3],"wb"))
		#total = confusion[key]["total"]
		#for act in confusion[key].keys():
		#	if act == "total":
		#		continue	
		#	confusion[key][act] = round(float(confusion[key][act])/float(total),2)"""