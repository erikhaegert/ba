import os
import torch
import pandas as pd
import pickle as pkl
import numpy as np
#import matplotlib.pyplot as plt
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
from sklearn.preprocessing import  OneHotEncoder
# Ignore warnings
import warnings
warnings.filterwarnings("ignore")


class ToTensor(object):
	"""Transformer class converts ndarrays in sample to Tensors."""
	def __call__(self, sample):
		#utterance, features, labels, masks, inv_map, length =  sample["utterances"], sample["features"], sample["labels"], sample["masks"], sample["inv_map"], sample["length"]

		features, cats, labels, masks, inv_map, length =  sample["features"], sample["cats"], sample["labels"], sample["masks"], sample["inv_map"], sample["length"]
		return {
				#'utterances': utterance,
				'features': torch.from_numpy(features),
				'cats': torch.from_numpy(cats),
				'labels': torch.from_numpy(labels),
				'masks': torch.from_numpy(masks),
				'inv_map': torch.from_numpy(inv_map),
				"length":length}

class GroundingStateDataSet(Dataset):
	"""Data handler Class. Turns annotations into data samples"""

	def __init__(self, annotations, transform=ToTensor()):
		self.annotations = pkl.load(open(annotations,"rb"))
		self.transform = transform
		self.encoder = OneHotEncoder()

	def __len__(self):
		return len(self.annotations.index.levels[0])-1

	def __getitem__(self, idx):
		utterances = np.array(self.annotations.loc[idx, "utterance"])
		masks = np.array(self.annotations.loc[idx, "mask"])
		states = self.annotations[["state"]].astype(int)		
		self.encoder.fit_transform(states)
		labels = self.annotations.loc[idx,"state"].apply(lambda x: self.encoder.transform(np.array(x).reshape(-1,1).astype(int)).toarray().flatten())
		inv_map = np.array(self.annotations.loc[idx,"state"].tolist(), dtype=int)
		
		features = self.annotations.loc[idx, "utt_embedding"]#.apply(np.array, axis=1)
		cats = self.annotations.drop(["utterance","state","utt_embedding", "acc_utterance", "mask"], axis=1).loc[idx,:].apply(np.array, axis=1)#sprk1
		cats = cats.apply(lambda x : np.hstack(x))
		cats = cats.apply(lambda x : x.astype(float))
		sample = {'features': np.array(features.values.tolist(), dtype=float), "cats": np.array(cats.values.tolist(), dtype=float),"labels": np.array(labels.tolist(), dtype=int), "masks": np.array(masks.tolist(),dtype=float), "inv_map":inv_map, "length":labels.shape[0]}
		#sample = {'utterances': utterances, 'features': np.array(features.values.tolist(), dtype=float), "labels": np.array(labels.tolist(), dtype=int), "masks": np.array(masks.tolist(),dtype=float), "inv_map":inv_map, "length":labels.shape[0]}

		if self.transform:
			sample = self.transform(sample)
		#return sample["utterances"], sample["features"], sample["labels"], sample["masks"], sample["inv_map"], sample["length"]

		return sample["features"], sample["cats"], sample["labels"], sample["masks"], sample["inv_map"], sample["length"]


if __name__ == "__main__":
	from collections import Counter
	c = Counter()
	d = GroundingStateDataSet("../data/latest_annotation.p")
	for i in range(len(d)):
		print(d[i][1])
			
	
	