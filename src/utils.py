import sys
import os

def create_data_list(path, postfix):
	data = []
	for dirpath, dirnames, filenames  in os.walk(path):
		for filename in filenames:
			if not filename.endswith(postfix):
				continue
			data.append(os.path.join(dirpath,filename))
	return data

