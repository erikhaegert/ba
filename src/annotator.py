import pandas as pd 
import os
import sys
import torch
import numpy as np
import utils as ut
import collections 
import itertools
from embedder import Embedder
from sklearn.preprocessing import LabelEncoder, OneHotEncoder, LabelBinarizer
from features import Features
from mask_generator import ActionMask
import pickle as pkl

class sliceable_deque(collections.deque):
	def __getitem__(self, index):
		if isinstance(index, slice):
			return type(self)(itertools.islice(self, index.start,
											   index.stop, index.step))
		return collections.deque.__getitem__(self, index)


class DUIterator():
	def __init__(self, data_path, features, sep= "\t", suffix = ".tsv", include_next_initI = True):
		self.next_init = include_next_initI
		self.data = ut.create_data_list(data_path, suffix)
		self.sep = sep
		self.features = features
		self.mask = ActionMask(self.next_init)
		self.uttdeque = sliceable_deque(maxlen=4)
		return 

	def __iter__(self):
		header = ["utterance", "speaker", "state", "grounding_act",  "acc_utterance", "is_same_speaker", "is_repetition", "is_addition", "prev_state", "role", "mask"]
		for d in self.data:
			with open(d) as infile:
				lines = infile.readlines()
				begin = 0
				for i in range(len(lines)):
					if "#" in lines[i]:
						continue
					#find beginning of next DU
					elif "InitI" in lines[i] and i != begin:
						uu_list = []
						
						prevspkr = "<start>"
						prev_state = "0"
						init = lines[begin].split(self.sep)[1]
						#iterate trhough DU
						for j in range(begin,i+1):
							print(lines[j], d)
							if "#" in lines[j]:
								continue
							lsplit = lines[j].split(self.sep)
							spkr = lsplit[1]
							if spkr != prevspkr and (prevspkr !="<start>" or (prevspkr =="<start>" and begin == 0)):
								#print(spkr,prevspkr)
								utt = lsplit[0].replace("äh ", "").replace("ähm ", "").replace("   ", " ").replace("  ", " ")
								#lsplit[0]=utt
								self.uttdeque.appendleft(utt)
								#print(utt, self.uttdeque)
							if spkr == prevspkr and j < i:
								utt = lsplit[0].replace("äh ", "").replace("ähm ", "").replace("hm ", "").replace("   ", " ").replace("  ", " ")
								if len(self.uttdeque)==0:
									self.uttdeque.appendleft(utt)
								else:
									self.uttdeque[0] += " " + utt
								#print(utt, self.uttdeque)
	
							
							state = lsplit[2]
							#umgang mit der tatsache, dass 4 fehlt
							#if int(state)>4:
							#	state = str(int(state)-1)
							if state == "#":
								continue
							if spkr == init:
								role = 0
							else:
								role = 1
							if self.next_init:
								if j < i:
									uu = lsplit + [self.uttdeque[0], self.features.is_same_speaker(prevspkr,spkr),self.features.is_repetition(self.uttdeque), self.features.is_addition(self.uttdeque),prev_state, role, self.mask(role, prev_state)[1:]]
								else:
									lsplit[2]="7"
									#lsplit[2]="6"
							uu = lsplit + [self.uttdeque[0],self.features.is_same_speaker(prevspkr,spkr),self.features.is_repetition(self.uttdeque), self.features.is_addition(self.uttdeque),prev_state, role, self.mask(role, prev_state)[1:]]
							uu_list.append(uu)
						
							prevspkr = spkr
							prev_state = state
						df_dict = {header[j]:[uu[j].strip() if type(uu[j]) == str else uu[j] for uu in uu_list] for j in range(len(header))}
						begin = i
						du = pd.DataFrame(df_dict)
						yield du




class Annotator():
	def __init__(self, tsv_path, include_next_initI=True, transform=None,):
		"""
        Args:
            tsv_file (string): Path to the tsv file with annotations.
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
		self.embedder = Embedder()
		self.features = Features()
		self.next_init = include_next_initI
		self.tsv_path = tsv_path
		self.discourse_units, self.grounding_acts = self.read_in_dus()
		

	def __getitem__(self, index):
		try:
			return self.discourse_units.loc[index,:]
		except:
			raise IndexError("Index {} out of range. Dataset only contains {} samples.".format(index, self.discourse_units.shape[0]))


	def read_in_dus(self):
		it = DUIterator(self.tsv_path, self.features, include_next_initI = self.next_init)
		du_list= []
		for du in it:
			#print(du)
			du["is_complete"] = du["acc_utterance"].apply(lambda x: float(self.features.is_complete(x)))
			du["is_declarative"] = du["acc_utterance"].apply(lambda x: float(self.features.is_declarative(x)))
			du["is_imperative"] = du["acc_utterance"].apply(lambda x: float(self.features.is_imperative(x)))
			du["yes_no"] = du["acc_utterance"].apply(lambda x: float(self.features.is_yes_no(x)))
			du["is_wh"] = du["acc_utterance"].apply(lambda x: float(self.features.is_wh(x)))
			du["is_alternative"] = du["acc_utterance"].apply(lambda x: float(self.features.is_alternative(x)))
			du["starts_with_conn"] = du["acc_utterance"].apply(lambda x: float(self.features.starts_with_conn(x)))
			du["is_explicit_req_ack"] = du["acc_utterance"].apply(lambda x: float(self.features.is_explitcit_request_for_ack(x)))
			du["present_for_check"] = du["acc_utterance"].apply(lambda x: float(self.features.present_for_check(x)))
			du["is_neg_aff"] = du["acc_utterance"].apply(lambda x: float(self.features.is_neg_or_aff(x)))
			du["is_eval"] = du["acc_utterance"].apply(lambda x: float(self.features.is_evaluation(x)))
			du["is_backchannel"] = du["acc_utterance"].apply(lambda x: float(self.features.is_backchannel(x)))
			du["utt_embedding"] = du["acc_utterance"].apply(lambda x: self.embedder.embed(x))		
			du_list.append(du)
		multi_df = pd.concat(du_list, keys= list(range(len(du_list))) ,axis=0)
		#multi_df = pd.concat([multi_df,multi_df.state.str.get_dummies()], axis=1)
		multi_df = pd.concat([multi_df,multi_df.speaker.str.get_dummies()], axis=1)
		multi_df = pd.concat([multi_df,multi_df.prev_state.str.get_dummies()], axis=1)
		gas = multi_df['grounding_act']
		del multi_df['grounding_act']
		del multi_df['speaker']
		return multi_df, gas




if __name__ == "__main__":
	path = sys.argv[1]
	data = ut.create_data_list(path, ".tsv")
	#for d in data:
	#	with open(d) as infile:
	#		for line in infile:
	#			if not line.startswith("#") and len(line.split("\t")) < 4:
	#				print(line)
	#				print(d)
	annotator = Annotator(path, include_next_initI=True)
	pkl.dump(annotator.discourse_units,open("../data/latest_annotation.p", "wb"))
	pkl.dump(annotator.grounding_acts ,open("../data/grounding_acts.p", "wb"))
	#prfloat(annotator.discourse_units.to_string())
	
	

