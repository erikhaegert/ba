import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.nn.utils.rnn as ut
import numpy as np
from torch.autograd import Variable

class GroundingStateHCN(nn.Module):
	""""HCN Class. Implements HCN model."""
	def __init__(
		self,
		feat_size, # size of total features
		cat_size,
		action_size, #number of states	
		batch_size,
		hidden_size=256,
		feat_selection = "all",
		use_mask = True
	):
		super(GroundingStateHCN, self).__init__()
		self.feat_size = feat_size
		self.cat_size = cat_size
		self.label_size = action_size
		self.batch_size = batch_size
		self.hidden_size = hidden_size
		self.feat_selection = feat_selection
		if self.feat_selection == "all":
			self.lstm = nn.LSTM(feat_size+cat_size, hidden_size, batch_first=True, dropout = 0.1)
		elif self.feat_selection == "linguistic":
			self.lstm = nn.LSTM(cat_size, hidden_size, batch_first=True, dropout = 0.1)
		elif self.feat_selection == "embeddings":
			self.lstm = nn.LSTM(feat_size, hidden_size, batch_first=True, dropout = 0.1)
		else:
			print("WARNIGN: seature_selection was set to {}! Only 'all', 'linguistic' or 'embeddings' possible!. Defaults to 'all'")
		if torch.cuda.is_available():
			self.lstm = self.lstm.cuda()
		self.fc = nn.Linear(hidden_size  , action_size)
		self.prediction = None
		self.logits = None 
		self.probs = None
		self.hidden = self.init_hidden(self.batch_size)
		self.use_mask = use_mask

	def init_hidden(self, new_batch_size):
		#print(torch.cuda.is_available())
		if torch.cuda.is_available():
			return (Variable(torch.zeros(1, new_batch_size, self.hidden_size).cuda()),
					Variable(torch.zeros(1, new_batch_size, self.hidden_size).cuda()))
		else:
			return (Variable(torch.zeros(1, new_batch_size, self.hidden_size)),
					Variable(torch.zeros(1, new_batch_size, self.hidden_size)))


	#takes padded batch, original sample lengths, padded bacth of masks 
	def forward(self, feats, cats, lengths, masks):#, hidden_out = False, hidden = None):
		self.hidden = self.init_hidden(cats.shape[0])
		batch_size, seq_len, _ = feats.size()
		#cats = cats.view(batch_size, seq_len, -1)
		if self.feat_selection == "all":
			usedfeats = torch.cat((feats, cats.double()), dim=2)
		elif self.feat_selection == "linguistic":
			usedfeats = cats.double()
		else:
			usedfeats = feats
		packedfeats = ut.pack_padded_sequence(usedfeats, lengths, batch_first=True)
		if torch.cuda.is_available():
			out, self.hidden = self.lstm(packedfeats.float().cuda(), self.hidden)
		else:
			out, self.hidden = self.lstm(packedfeats.float().cuda(), self.hidden)
		out, _ = ut.pad_packed_sequence(out, batch_first=True)
		out = out.contiguous()
		out = out.view(-1, out.shape[2])
		self.logits = self.fc(out)
		self.logits = self.logits.view(batch_size, seq_len, self.label_size)
		self.probs = F.softmax(self.logits, dim=1)
		self.probs = self.probs.view(batch_size, seq_len, self.label_size)
		if self.use_mask == True:
			
			if torch.cuda.is_available():
				self.probs = F.normalize(self.probs.double().cuda() * masks.double().cuda(), p=1, dim=2)
			else:
				self.probs = F.normalize(self.probs.double() * masks.double(), p=1, dim=2)
		else:
			if torch.cuda.is_available():
				self.probs = F.normalize(self.probs.double().cuda(),  p=1, dim=2)
			else:
				self.probs = F.normalize(self.probs.double(), p=1, dim=2)

		predictions = torch.argmax(self.probs, dim = 2)+1
		return predictions

	









