Versuchspersonen-Paar 8
""ahm, du hast da ganz viele tolle Teile da liegen, ne"
Instrukteur (I) m; Konstrukteur (K) m
Bedingung: Sicht eingeschr"ankt, Vorlage Modell
Gesamtdauer 19: 05
Konstruktionsresultat I:
Aa=10.3Ab=08.1Ba=10.1Bb=08.2C=02.1Da=12.2Db=01.1Dc=08.3Ea=12.1
Eb=01.2Ec=07 Ed=05 Ee=13 Ha=11.3Hb=02.2Hc=04 Hd=03.1He=11.1
La=14.1Lb=15.1Lc=09.2Ld=16.1Le=17.1Ra=14.2Rb=15.2Rc=09.1Rd=16.2
Re=17.2Va=11.2Vb=03.2Vc=06Vd=03.3Ve=03.4Vf=10.2Muttern=VR
Anmerkung: Video beginnt mit Zoom-in.
Konstruktionsresultat K:
Aa=10.3Ab=08.1Ba=10.1Bb=08.2C=02.1Da=12.2Db=01.1Dc=08.3Ea=12.1
Eb=01.2Ec=07 Ed=05 Ee=13 Ha=11.3Hb=02.2Hc=04 Hd=03.1He=11.1
La=14.1Lb=15.1Lc=09.1Ld=16.1Le=17.1Ra=14.2Rb=15.2Rc=09.2Rd=16.2
Re=17.2Va=11.2Vb=03.2Vc=06Vd=03.3Ve=03.4Vf=10.2Muttern=VR
Zun"achst keine C-Verl"angerung, zun"achst E-Baugruppe zwischen Ba und da.

08I001	jetzt geht(_e)s los <-> "ahm, du hast da ganz viele tolle Teile da liegen, ne?
08K001	mhm.
08I002	dann nimm doch mal als erstes <-> diese zwei langen *Holz_sch_sch_leisten mit den <-> sieben L"ochern
08K002	mhm.
08I003	und leg(e) <noise> die an {die Seite}<spk: I, die l"angsten> genau. <hum: lachen> <-> und dann packst du die am besten, <-> dieses mit eins, zwei, drei, vier, f"unf L"ochern <--> </noise: klappern>
08K003	mhm.
08I004	eins mit drei L"ochern.
08K004	<noise: rascheln> ja.
08I005	und nimmst die <-> zwei gelben Schrauben, die eine mit <-> <noise: klappern> mit eckig Baufix und die mit dem runden Kopf.
08K005	mhm.
08I006	so, und dann <-> schraubst du jetzt die mit den drei L"ochern <hum: r"auspern> so als Verl"angerung von der mit den f"unf L"ochern <-> mit <noise> den zwei gelben an, wobei allerdings </noise: klappern> die Runde <noise: ?> am Ende von dem mit den f"unf L"ochern ist, verstehst du?
08K006	mit zwei Schrauben zusammen?
08I007	mit zwei Schrauben hinten, <noise: klappern> da"s also nur noch ein Loch von dem mit den drei L"ochern "uberbleibt.
08K007	ja.
08I008	und die <-> Runde ist also am Ende von der mit f"unf L"ochern und <noise: klappern> die mit dem eckigen Kopf
08K008	am Ende mit <-> <par> mit allen dreien </par: 1>
08I009	<par> ist an </par: 1> <-> mehr zur Mitte hin, also <-> zweiten Loch.
08K009	mhm. <noise: ?>
08I010	"ahm und unten drunter nimmst du <noise> dann die orangenen <-> Muttern, <--> die so <-> rautenf"ormig </noise: klappern> sind. {<sil: 10>}<noise: klappern>
08K010	jau.
08I011	jau, toll. also hast du jetzt irgendwie so(_ei)n verl"angertes Ding, ne?
08K011	genau.
08I012	super, jetzt nehmen <subst> wir </subst: wa> <--> nehmen <subst> wir </subst: wa> <hum: lachen> <-> <attrib> <quest: hier sind> diese neuen hier <-> </attrib: leise> "ahm m <--> eine rote Schraube <hum: husten> mit <noise> eckigen Kopf <--> </noise: klappern> und die und dieser rote, gro"se W"urfel
08K012	mhm.
08I013	<hum: r"auspern> und den baust du jetzt <--> "ahm die rote Schraube von unten, also da, wo die orangenen Schrauben sind <sil: 2> die sind doch hoffentlich auf einer Seite?
08K013	die Muttern?
08I014	die Muttern <-> ja.
08K014	ja.
08I015	und jetzt "ahm auf der Seite steckst du jetzt die <-> die die die die <-> <par> rote Mutter </par: 2>
08K015	<par> rote Schraube </par: 2> <par> Schraube </par: 3>
08I016	<par> eine mit </par: 3> dem eckigen Kopf, Schraube, genau.
08K016	<hum: lachen> ja.
08I017	und packst diesen roten, gro"sen W"urfel da drauf.
08K017	wohin denn?
08I018	"ahm, da"s der so nach oben ab steht <--> ah, solche Fragen. du hast bestimmt.
08K018	also ich stecke die Schraube von unten jetzt, wenn, ich habe die <-> ro/ "ah orangenen <-> Muttern nach, die zeigen jetzt nach oben. <sil: 3>
08I019	nee, also das Gelbe soll oben sein. mach(e) mal <quest: ?> einfach so <par> fest </par: 4>
08K019	<par> ja, </par: 4> so. {also}<spk: I, "ahm> hinterher soll der rote <-> W"urfel
08I020	auch nach oben zeigen.
08K020	auch nach oben zeigen.
08I021	hast du denn {dieses Dreierbrett,}<spk: K, in welches Loch?> hast du das <-> oben auf das F"unfer gelegt oder unten drunter?
08K021	oben.
08I022	ja, das ist nat"urlich eigentlich schade <noise: klappern>
08K022	ja, dann mache ich es anders {<sil: 9>}<klappern>
08I023	<noise> schwierig, schwierig. <sil: 14> </noise: klappern>
08K023	<noise> so, jetzt soll der rote W"urfel oben drauf, in welches Loch? </noise: klappern>
08I024	<noise> "ah, von dem Dreier halt, das eine, was "ubrig bleib </noise: klappern>
08K024	<noise> ach das letzte {Loch}<spk: I, mhm> ja. </noise: klappern>
08I025	hei"st also, da"s du dann diesen roten W"urfel, dann kommt die <--> gelbe <-> Schraube <-> die ro/ gelbe, runde {Schraube}<spk: K, mhm> und dann die gelbe Baufixschraube <hum: lachen>
08K025	alles klar, <-> ja, hab(e) ich.
08I026	super <-> und jetzt *schieckst du den <-> eine F"unferquerleiste <noise: klappern> so eine
08K026	ja.
08I027	die kommst jetzt oben auf diesen roten <noise> <-> W"urfel raus mit einer roten, runden Schraube <sil: 3> </noise: klappern>
08K027	nach rechts oder nach links?
08I028	einfach richtig nach oben, also wenn es
08K028	ach so, nur verl"angern?
08I029	nee, nicht verl"angern, sondern quer <-> neunzig Grad dazu, {also}<spk: K, ja> es steht
08K029	nach rechts <noise> oder mittig oder nach links? <--> </noise: rascheln> {also}<hum: atmen> welches Loch mu"s ich <-> <par> von diesem F"unfer nehmen? </par: 5>
08I030	<par> nee, in den roten W"urfel </par: 5> einfach so oben drauf, <noise> da"s das Ding <-> "ahm <-> </noise: rascheln>
08K030	rechts und lich/ links gleich lang?
08I031	ja, in der Mitte. mhm.
08K031	{okay.}<noise: klappern>
08I032	<noise> hei"st also, da"s zwei L"ocher zu jeder Seite "uberstehen </noise: klappern> <hum> das ganze Ding </hum: lachen> steht senkrecht dazu.
08K032	ja <--> gut.
08I033	mit der roten, runden Schraube? <hum: lachen>
08K033	ja.
08I034	super, jetzt hast du so (ei)n Heck, ne?
08K034	mhm.
08I035	so "ahm jetzt wird es spannend und schwierig. <hum: lachen> jetzt nimmst du nochmal so eine Dreierleiste
08K035	ich habe noch keine genommen, aber ist egal.
08I036	doch vorhin schon mal.
08K036	ja, stimmt <hum: lachen>
08I037	und "ahm legst die "ahnlich wie die vorhin hin, die du mit den gelben Schrauben festgemacht hast
08K037	{"ahnlich}<noise: klappern> <par> nur auf die andere Seite? </par: 6>
08I038	<par> wie da unten </par: 6>
08I038	mhm.
08K038	ja.
08I039	nur auf die andere Seite <-> und nimmst zwei orangene Schrauben <-->
08K039	na ja, die <par> runde </par: 7>
08I040	<par> und pa/ </par: 7> ja, wenn du die Runde jetzt gerade hast, dann packst du die <noise: klappern> "ahm in das zweite Loch der <-> F"unferlange in der Mitte, <-> also <--> nicht F"unferlang in der Mitte ist gut <-> "ahm <hum: lachen> das zweite Loch von der F"unfer, <noise: klappern> jetzt vom anderen Ende, ne?
08K040	{ja <-->}<noise: klappern>
08I041	und steckst dann von unten halt die Dreierleiste da dran <-> {hei"st also}<spk: K, mhm> jetzt geht die Verl"angerung in die andere Richtung.
08K041	ja.
08I042	und machst die mit einer orangenen Schraube fest <-> nee, Moment, jetzt habe ich den Fl"ugel vergessen <-> und "uber die <sil: 2> F"unferleiste, die Lange, die wir jetzt haben
08K042	ja.
08I043	da kommt dann jetzt auch quer wie der Heckfl"ugel, allerding direkt da drauf <-> {"ahm}<noise: klappern> <sil: 3>
08K043	(ei)n(e) F"unfer oder?
08I044	nee, ja eine F"unferlangeleiste, so, nee Sechser, Siebener, Siebener ist das.
08K044	Moment, <--> <hum: lachen> du sprichst jetzt immer von der F"unferleiste?
08I045	ja, auf die F"unferleiste kommt jetzt so eine Siebenerleiste quer <-> also die steht dann parallel zu der <--> F"unfer, die du vorhin dem roten W"urfel angeschraubt hast <->
08K045	<noise: klappern> ach so. <->
08I046	ach so. {<sil: 3>}<noise: klappern>
08K046	Moment.
08I047	kann ich ihm eigentlich auch sagen, worum es geht? <-> ja gut, dann <-> werden wir da irgendwann (ei)n Flugzeug bekommen, das ist da <-> wird die Sache vielleicht schon klarer. <hum: lachen>
08K047	also <-> jetzt nochmal. ich habe jetzt die F"unferleiste da oben drauf geschraubt mit der roten <-> Schraube. jetzt habe ich noch eine F"unferleiste. <--> weil eben hast du n"amlich was von einer Dreierleiste erz"ahlt.
08I048	ja, die Dreier, <-> die <-> die andere F"unfer <-> m/ <-> du hast keine andere F"unfer mehr.
08K048	ich habe noch eine F"unfer, die habe ich noch nicht angebaut. <--> du bist jetzt gesprungen von Dreier auf F"unfer <-> also wo mu"s jetzt die F"unferleiste hin? <noise: ?>
08I049	<par> du hast ja </par: 8>
08K049	<par> und die </par: 8> Dreierleiste habe ich auch noch nicht <--> <par> angebaut </par: 9>
08I050	<par> du hast </par: 9> nur zwei F"unferleisten, ne.
08K050	ja. und eine habe ich jetzt quer zu der Langen <--> und wo kommt die n"achste hin?
08I051	quer zu der F"unfer vorhin <--> sollten wir uns <-->
08K051	die <-> also zwei <-> <par> F"unfer? </par: 10>
08I052	<par> du hast </par: 10> vorhin noch eine F"unfer genommen, und da hast du im Endeffekt unten einen Dreier dran gemacht und da mit dem roten W"urfel <-> quer dazu eine F"unfer, so da"s man, wenn man von oben drauf gucken w"urde, (ei)n T hat.
08K052	ja, aber ich habe die <-> "ah Siebener {genommen}<spk: I, ja> also Siebener mit Dreierverl"angerung.
08I053	nee, das war schade. <hum: lachen>
08K053	<noise: klappern> ich habe am Anfang noch die Siebenerverl"angerung.
08I054	nein, das das du hast
08K054	die Siebener habe ich noch gar nicht <par> sollte ich noch gar nicht verbaut haben? </par: 11>
08I055	<par> die Siebener sollten ja weg </par: 11> da habe ich ja gesagt, die zwei Siebener an die Seite legen.
08K055	ach, an die <noise> Seite legen, <-> das hei"st <-> ja, alles klar. </noise: klappern> nee, ich habe
08I056	weil das ergeben nachher die Fl"ugel.
08K056	ach so.
08I057	super <noise> <sil: 4> <hum: lachen> </noise: klappern>
08K057	aber die Runde war <-> "ahm <--> mehr zum W"urfel hin, ne, die runde, Gelbe?
08I058	mhm, <noise> die Runde war am Ende. <--> und dann kommt die <-> Eckige </noise: klappern>
08K058	ja, jetzt hab(e) ich <-> eine F"unfer mit einer Dreier verl"angert <-> oben am Ende den roten W"urfel drauf, quer dazu
08I059	auf dem Dreier?
08K059	auf dem Dreier und quer da <-> zu dem <--> zu der Verl"angerung habe ich jetzt noch (ei)n(en) F"unfer <-> mit roter Schraube oben drauf.
08I060	mhm, hei"st also, <quest: wenn man da so von oben> guckt, (ei)n T <noise: klappern>
08K060	genau <hum: atmen>
08I061	gut <-> "ahm und jetzt kommt halt an dieses <noise: rascheln> F"unferende, <--> {was das}<noise: rascheln> lange T bilden w"urde, das lange T-St"uck.
08K061	ja.
08I062	da kommt jetzt in das zweite Loch <noise: ?> <--> von oben <noise: klappern> was noch frei ist <sil: 3>
08K062	{ja}<noise: mikro>
08I063	da kommt eine orangene Mutter rein, aber da drunter kommt <noise: klappern> dieses Siebenerlangst"uck, also das ist jetzt der erste Fl"ugel <noise> <sil: 2> dann ist kein T mehr </noise: klappern> <par> <noise> sondern </noise: klappern> </par: 12>
08K063	<par> drunter </par: 12> <noise> kommt das? </noise: klappern>
08I064	<noise> nee, dr"uber, also auf das <--> </noise: klappern>
08K064	<par> auf das </par: 13>
08I065	<par> auf der </par: 13> Ebene,<-> wo auch die die Muttern, die gelben Muttern drauf sind.
08K065	mhm
08I066	also du hast <noise: klappern> an den einen Ende die runde Gelbe die <-> eckige Gelbe, dann kommt (ei)n leeres Loch.
08K066	mhm <par> dann die runde orange? </par: 14>
08I067	<par> kommt der </par: 14> Siebenerfl"ugel und da geht der ja <-> <hum: r"auspern> die runde orange drunter, sie wird mit einer orangenen Schraube festgemacht.
08K067	drunter? <-> die Schraube stecke ich jetzt von unten <par> nach? </par: 15>
08I068	<par> nein </par: 15> <-> nein, nein, nein <-> die orange Schraube kommt von oben rein, und unten drunten ist halt die <-> gelbe Mutter <-> ja die orange Mutter. <hum: schniefen>
08K068	<attrib> ach so </attrib: leise> <noise> <sil: 3> <hum: lachen> <sil: 3> </noise: rascheln> ja.
08I069	ja, und dann in dem Loch davor kommt die <-> eckige, orangene Mutter mit dem zweiten Siebenerfl"ugel {<sil: 4>}<noise: klappern>
08K069	selbe Prinzip?
08I070	selbe Prinzip, nat"urlich in der Mitte, das ist fast schon anzunehmen, ne <--> haben wir vorhin nicht gesagt, <par> aber </par: 16>
08K070	<par> ja, ja </par: 16> <-> nee, ist klar.
08I071	<par> hast du? </par: 17>
08K071	<par> ja, </par: 17> mhm.
08I072	<par> ja, und diese </par: 18>
08K072	<par> jetzt habe ich aber </par: 18> keine Mutter mehr.
08I073	nee, nee, wird <hum: lachen> auch mit dem <--> gr"unen W"urfel festgemacht.
08K073	mhm <noise: klappern>
08I074	<noise> und zwar so, da"s <-> die haben ja denn immer </noise: klappern> verschiedende Bohrungen da, wo keine Gewinde sind, {<sil: 2>}<noise: klappern>
08K074	ja.
08I075	das die <-> irgendwie so nach unten st/ also zur Seite wegstehen, <-> da"s, wenn man also von der Seite jetzt durchguckt.
08K075	mhm <-> <noise: klappern>
08I076	<noise> die <-> gro"sen Bohrungen ohne Gewinde <--> "ahm in der Achse stehen wie die Fl"ugel auch sind <sil: 5> </noise: klappern>
08K076	<noise> jetzt hast du mich nat"urlich eingequetscht mit den ganzen Muttern <hum: lachen> <-> ja </noise: klappern>
08I077	<noise> ja, da mu"s man die Orangenen manchmal (ei)n bi"schen wegschieben, ne </noise: klappern>
08K077	{mhm}<noise: klappern>
08I078	aha, super.
08K078	so. <noise: klappern>
08I079	<noise> so <-> und an den gr/ unter den gr"unen W"urfel <--> hast du? </noise: klappern>
08K079	mhm.
08I080	unter den gr"unen W"urfel <-> <noise: klappern> "ahm schraubst du jetzt <-> den gelben W"urfel, und zwar mit der langen, gr"unen Mutter <noise: klappern> also so da"s die beiden W"urfel aufeinanderstehen.
08K080	und die <--> ohne Gewinde <-> zeigen in dieselbe Richtung?
08I081	hm <par> <quest: lass(e) mich mal "uberlegen> </par: 19>
08K081	<par> also zur Seite </par: 19> weg, wenn ich <par> jetzt mal? </par: 20>
08I082	<par> nee, </par: 20> das nicht, das ist jetzt genau umgekehrt, da"s jetzt die ohne Gewinde, da steckst du die gr"une Mutter durch, diese gr"une lange, <sil: 4> da"s du <-> "ahm <--> in der Achse <noise: klappern> <-> ja, dieses Fl"ugels, <noise: klappern> der oben dr"uber ist <-> "ah Gewinde diesmal hast, weil da kommen gleich die R"ader dran, <-> das hei"st die mu"st du ja irgendwie festschrauben.
08K082	ach so, also <-> die <-> das Loch ohne Gewinde zeigt jetzt nach unten, und da stecke ich dann
08I083	{die Mutter durch.}<noise: klappern>
08K083	<noise> die Mutter durch <attrib> und dreh das fest, gut </attrib: leise> </noise: klappern>
08I084	<noise> <quest: hei"st also> dann steht der gelbe W"urfel unter dem gr"unen, </noise: klappern>
08K084	{mhm}<noise: klappern>
08I085	unten die gr"une Mutter <--> so, und jetzt <-> "ahm k"onnen wir mal flugs eben erstmal die R"ader zusammenbauen <noise: klappern> <-> steckst also jeweils in einen wei"sen <-> Laufring, <noise: klappern> diese Reifen <-> <noise> die roten Scheiben rein <sil: 9> </noise: klappern>
08K085	jau. <noise: klappern>
08I086	"ahm <noise> <--> dann sind da noch so so hellwei"se Plastikdinger </noise: rascheln>
08K086	mhm <noise: klappern>
08I087	<noise> die geh"oren <--> in die Dinger, </noise: klappern> <-> also in die {roten Scheiben}<noise: klappern> in die Felgen <hum: lachen>
08K087	mhm. {also nach}<spk: I, aber so> au"sen die
08I088	aber so, <--> da"s die Kanten nach au"sen gehen.
08K088	ja.
08I089	ja und dann schraubst du halt mit den <noise> <sil: 2> zwei <sil: 3> </noise: klappern> <hum: schniefen> zwei blauen Muttern, wobei die <--> ha, jetzt wird(_e)s {schwierig.}<spk: K, also> wenn man von oben guckt auf das Ding <--> und der Heckfl"ugel da mit dem roten W"urfel zeigt zu dir hin,
08K089	ja.
08I090	<noise> dann soll auf die rechte Seite die runde, blaue Mutter {rein}<spk: K, mhm> </noise: mikro> Schraube. <noise> <attrib> verwechsel immer <hum: lachen> ist echt schon </attrib: leise> <sil: 4> </noise: rascheln>
08K090	ja.
08I091	auf der <-> linken sollte dann die <noise: rascheln>
08K091	<attrib> das Andere </attrib: leise>
08I092	Eckige. <--> und dazwischen, das habe ich nat"urlich jetzt wieder vergessen. hast du schon gemacht?
08K092	ja.
08I093	diese lila Distanzringe, sonst pa"st das bestimmt auch gar nicht toll.
08K093	ja, nee, pa"st auch nicht <hum: lachen> <noise: rascheln>
08I094	<noise> ja, siehst du. <sil: 22> </noise: rascheln>
08K094	jau. <noise: klappern>
08I095	{jau <sil: 3>}<noise: rascheln>
08K095	mhm.
08I096	und <noise> jetzt kommt <sil: 4> </noise: rascheln>
08K096	mu"s das Spiel haben oder?
08I097	ja, die R"ader <hum: lachen> k"onnen sich drehen, ne.
08K097	{nein, ich meine}<spk: K, wenn du das richtig gemacht hast> die Achse da unten, nee, ne? <-> ah, ist egal <-> machen wir mal {weiter}<hum: lachen>
08I098	und jetzt pa"st ja noch <-> auf diesem vorderen Dreier, wo die beiden Fl"ugel sind <sil: 4> da pa"st ja jetzt nur noch der blaue W"urfel hin, ne?
08K098	<noise: klappern> <noise> "ahm ich habe jetzt noch drei Dreier, ne? </noise: rascheln>
08I099	ja, klar.
08K099	gut. ja, vorne pa"st noch (ei)n W"urfel hin.
08I100	nee, drei Dreier hast du noch?
08K100	ja <sil: 3>
08I101	du machst es echt spannend <hum: lachen> wo hast du denn den jetzt her?
08K101	wo mu"ste der Dreier denn hin?
08I102	der mu"ste vorne an diesen F"unfer als Verl"angerung dran in die andere Richtung.
08K102	genau wie der Erste?
08I103	genau wie der Erste.
08K103	aha.
08I104	<hum: lachen> hei"st also, du mu"st dann die gelbe <-> Mutter <hum: r"auspern> und den <-> gr"unen W"urfel nochmal losmachen.
08K104	die orange Mutter.
08I105	ja, gut <par> <quest: ?> </par: 21>
08K105	<par> den gr"unen </par: 21> <noise> W"urfel mu"s ich gar nicht losmachen. </noise: klappern>
08I106	{auch.}<noise: klappern>
08K106	{warum?}<noise: klappern> es m"ussen doch nur zwei <-> mu"s die denn gesamt drunterliegen?
08I107	nicht gesamt, sondern da"s noch ein Loch freibleibt.
08K107	ja.
08I108	<noise> hei"st also, die <--> orange, runde Schraube und die <-> orange, eckige Schraube </noise: klappern> gehen durch diesen F"unfer, der l"angs ist, unten durch zwei L"ocher von dem Dreier.
08K108	ach so, schei"se <noise> <--> <hum: lachen> na ja, gut <-> jetzt wird(_e)s schwieriger <sil: 8> <hum: lachen> na gut <sil: 24> <hum: lachen> {na}<attrib: leise> <sil: 15> {na}<attrib: leise> <hum: lachen> <sil: 3> </noise: rascheln>
08I109	<noise> kannst ja mal erz"ahlen, wie das ganze aussieht <sil: 3> </noise: klappern>
08K109	{sofort.}<noise: klappern> <hum: lachen>
08I110	<noise> w"urde mich ja mal schwer interessieren. </noise: klappern>
08K110	<hum: r"auspern> <hum: schniefen> <sil: 4> der zweite Fl"ugel, <--> also ich habe jetzt <--> wenn ich jetzt das Heck hin, also wenn das zu <noise: klappern> mir zeigt.
08I111	mhm.
08K111	dann habe <noise> ich den Ersten <-> also erstmal habe ich eine <-> zwei gelbe Schrauben, dann ist ein Loch. </noise: klappern>
08I112	mhm.
08K112	dann habe ich die orange Eckige mit den Fl"ugeln dran.
08I113	die orange runde.
08K113	runde schei"se <hum: lachen> <noise: klappern> <noise> <--> ach Mist. <sil: 5> </noise: rascheln>
08I114	mit der orangenen Mutter, ne? <sil: 3>
08K114	<hum: lachen> nee, ich habe dadrunter den gr"unen W"urfel. <hum: lachen>
08I115	nee, da mu"s aber die orangene Mutter drunter
08K115	oh.
08I116	<hum: lachen> <quest: ist schon wieder> k"oniglich.
08K116	wo kommt denn dann dieses Ganze
08I117	Ged"ons mit dem Rad?
08K117	ja.
08I118	ja, das kommt einen davor <-> dann kommt die orange, eckige Mutter <--> mit dem
08K118	also doch <par> noch </par: 22>
08I119	<par> Fl"ugel </par: 22> der davor liegt.
08K119	also <--> dieses Gest/ <-> dieses Rad kommt also direkt nach der gelben Mutter <--> "ah nach der gelben Schraube? da ist kein Loch mehr zwischen
08I120	doch <-> aber das kommt halt unter den.
08K120	das mache ich jetzt zu
08I121	nein, <-> es kommt die <-> <quest: un/> wenn man jetzt vom Heck her guckt, hast {du}<spk: K, ja> wenn man von oben guckt, dieses <-> die rote, runde Mutter
08K121	ja.
08I122	die gelbe, runde Mutter
08K122	ja.
08I123	die <hum> eckige, gelbe Mutter </hum: lachen>
08K123	ja.
08I124	(ei)n Loch.
08K124	ja.
08I125	die <-> orangene, runde Mutter <-> mit dem ersten Fl"ugel <-> diesen <par> ersten Siebener, der quer ist </par: 23>
08K125	<par> ja, das hatte ich doch so </par: 23> ja, und was?
08I126	da drunter ist die orange Mutter <noise: klappern> <--> und dazwischen <noise: klappern> ist halt noch dieses Dreierst"uck <noise: klappern>, ne also da geht man auch schon durch <-->
08K126	ja, ich habe jetzt aber dann noch zwei
08I127	zwei L"ocher nach vorne hin frei
08K127	mhm.
08I128	<noise> ja, und dann kommt in das n"achste die eckige, orangene Mutter </noise: rascheln> dadrunter liegt der Fl"ugel <--> geht durch das F"unferl"angsst"uck und die Dreierverl"angerung nach vorne.
08K128	wo ist denn jetzt mein ganzes <-> "ah Rad, in welchem <par> Loch? </par: 24>
08I129	<par> ja, </par: 24> das kommt an die orangene Mutter dran <-> <noise: klappern> mit dem <-> die orange, eckige Mutter mit dem <-> gr"unen W"urfel.
08K129	die <hum: r"auspern> und wo kommt der zweite Fl"ugel hin, auch <par> da </par: 25>
08I130	<par> der </par: 25> kommt da dr"uber <--> also der <noise> liegt unter der eckigen, orangenen </noise: rascheln> Mutter
08K130	{unter}<spk: I, ?>? ist das andere auch unter gewesen?
08I131	jetzt mach(e) mich nicht <hum: lachen> <attrib> ist ja </attrib: leise> unter der <hum: r"auspern> <-> der der Schraube, aber <--> es liegt, die beiden Fl"ugel liegen "uber diesem F"unferquerst"uck, also {du hast "uberhaupt}<spk: K, ja dann <-> dann ist gut> die Hauptachse gebildet.
08K131	ja, dann habe ich <--> also es kommen zwei Fl"ugel direkt hintereinander?
08I132	<quest: ?> genau. <attrib> {das}<spk: K, ja> , <noise> das wird schon langsam was </noise: rascheln> </attrib: leise>
08K132	einmal mit runder und einmal <par> mit </par: 26>
08I133	<par> einmal </par: 26> mit eckiger. <noise> und die Eckige wird festgeschraubt <-> in diesem gr"unen W"urfel. und der liegt <-> unter diesem F"unfer_<->_l"angsst"uck und <-> unter dem F"unferdrei/, nee unter der Dreierverl"angerung, die </noise: rascheln> <par> nach vorne geht. </par: 27>
08K133	<par> ja, ich glaube </par: 27> <noise> jetzt habe ich (e)s </noise: rascheln> <-> also <-> rot, gelb <hum: lachen> gelb, leer <--> orange, rund mit "ahm Mutter {drunter}<spk: I, mhm> orange eckig mit <-> "ahm den ganzen <-> Rad da drunter.
08I134	ja, und dann hast du noch ein Loch nach vorne <par> frei </par: 28>
08K134	<par> einen </par: 28> habe ich n/ <-> <par> frei. so jetzt kommt <-> kommt der blaue W"urfel </par: 29>
08I135	<par> super, und mit der eckigen roten Mutter </par: 29> <-> der blaue W"urfel drunter festgeschraubt.
08K135	{drunter noch?}<noise: rascheln>
08I136	mhm <sil: 2>
08K136	mit der Roten? <noise: klappern>
08I137	{mhm <sil: 9>}<noise: klappern>
08K137	<noise> jetzt habe ich immer noch zwei <--> "ahm Drei/ </noise: rascheln>
08I138	<noise> {Dreier}<spk: K, Dreier> ja und da kann man ja toll einen Propeller raus machen, wenn man die "uber Kreuz vorne in {den}<spk: K, stimmt> blauen W"urfel reinschraubt. </noise: rascheln>
08K138	{jau.}<noise: klappern>
08I139	<noise> dazu mu"s nat"urlich beim blauen W"urfel die <--> {<quest: ?>}<spk: K, mu"s ich nochmal drehen> mit dem Gewinde nach vorne zeigen, ne? </noise: klappern>
08K139	{ja <sil: 7>}<noise: klappern>
08I140	{<quest: ?> <sil: 7>}<noise: klappern>
08K140	{ja ha.}<noise: klappern>
08I141	<noise> jetzt solltest du (ei)n richtig tolles Flugzeug haben, ne? </noise: klappern>
08K141	ja, habe ich auch.
08I142	ja, habe ich auch <hum: r"auspern> super.
08K142	(ei)nen Doppeldecker <-> fast.
08I143	fast, {genau}<attrib: leise> hintereinander Fl"ugel Ende? <--> Ende gut, alles gut.
