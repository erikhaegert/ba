for i in 1 2 3 4 5 6
	do
	countb="$(egrep  "\t$i\t" split_conditions/blocked/* | wc -l)"
	allb="$(grep  "\t[1-6]\t" split_conditions/blocked/* | wc -l)"
	percentb=$(bc <<<"scale=4;$countb/$allb*100")
	countr="$(egrep  "\t$i\t" split_conditions/restricted/* | wc -l)"
	allr="$(grep  "\t[1-6]\t" split_conditions/restricted/* | wc -l)"
	percentr=$(bc <<<"scale=4;$countr/$allr*100")
	both=$(bc <<<"scale=4;$countb+$countr")
	allboth=$(bc <<<"scale=4;$allb+$allr")
	pboth=$(bc <<<"scale=4;$both/$allboth*100")
	echo "$i&$countb&$percentb&$countr&$percentr&$both&$pboth\\\\"
done
echo "§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§"
initb="$(egrep  "InitI" split_conditions/blocked/* | wc -l)"
initr="$(egrep  "InitI" split_conditions/restricted/* | wc -l)"
pinitb=$(bc <<<"scale=4;$initb/$allb*100")
pinitr=$(bc <<<"scale=4;$initr/$allr*100")
both=$(bc <<<"scale=4;$initb+$initr")
pboth=$(bc <<<"scale=4;$both/$allboth*100")
echo "InitI&$initb&$pinitb&$initr&$pinitr&$both&$pboth\\\\"
declare -a arr=("Cont" "Rep" "ReqAck" "ReqRep" "Ack" "Can")
for act in "${arr[@]}"
	do
	I="${act}I"
	R="${act}R"
	actIb="$(egrep  "$I" split_conditions/blocked/* | wc -l)"
	actRb="$(egrep  "$R" split_conditions/blocked/* | wc -l)"
	actIr="$(egrep  "$I" split_conditions/restricted/* | wc -l)"
	actRr="$(egrep  "$R" split_conditions/restricted/* | wc -l)"
	pIb=$(bc <<<"scale=4;$actIb/$allb*100")
	pRb=$(bc <<<"scale=4;$actRb/$allb*100")
	pIr=$(bc <<<"scale=4;$actIr/$allr*100")
	pRr=$(bc <<<"scale=4;$actRr/$allr*100")
	allI=$(bc <<<"scale=4;$actIb+$actIr")
	allR=$(bc <<<"scale=4;$actRb+$actRr")
	pbothI=$(bc <<<"scale=4;$allI/$allboth*100")
	pbothR=$(bc <<<"scale=4;$allR/$allboth*100")
	echo "$I&$actIb&$pIb&$actIr&$pIr&$allI&$pbothI\\\\"
	echo "$R&$actRb&$pRb&$actRr&$pRr&$allR&$pbothR\\\\"
	done