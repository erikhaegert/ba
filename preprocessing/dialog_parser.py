import sys
import os
import re
import spacy
from nltk.tokenize import sent_tokenize


class DialogParser():

	def __init__(self,spacy_model = 'de_core_news_md'):
		self.tagger = spacy.load(spacy_model)
		return 

	def remove_noises(self, line):
		line = re.sub(r"<(\/?pa|\\?pa|\/?hu|\/?con|\/?nois|nois|\/?atri|\\?atri|\/?attri|\\?attri|\/?subs)[^>]+>|<quest:[ \?]+>|<\/quest:[ \?]+>|<spk: [KI],[ \?]+>|_|\(|\)|\{|\}", "",line)
		line = re.sub(r"^{<sil[^>]+>}|^<sil[^>]+>", "", line)
		return line

	def handle_unlauts(self, line):
		line = re.sub(r"\"s", "ss",line)
		line = re.sub(r"\"[aA]", "ä",line)
		line = re.sub(r"\"[oO]", "ö",line)
		line = re.sub(r"\"[uU]", "ü",line)
		return line


	def first_pass(self,lines):
		new_lines = []
		for line in lines:
			if re.search(r"[0-9]{2}[IK][0-9]{3}", line):
				line = self.remove_noises(line)
				line = self.handle_unlauts(line)
				new_lines.append(line.lower())
			else:
				continue
		return new_lines



###################################
	def second_pass(self, lines):
		out_lines = []
		#print("SECOND PASS: ", lines)
		for line in lines:
			tokenized_segment = self.tokenize_line(line)
			if tokenized_segment is None:
				continue
			out_lines = out_lines + tokenized_segment
		return out_lines



	def tokenize_line(self, line):
		tokenized_segment = []
		spkr, utt = line.split("\t")[0], line.split("\t")[1]
		if "v" in spkr:
			return
		elif "i" in spkr:
			spkr = "spkr1"
		else:
			spkr = "spkr2"
		sents = sent_tokenize(utt)
		for sent in sents:
			if re.search(r"<-+>|<sil[^>]+>", sent):# or re.search(r"<sil[^>]+>", sent):
				split_utt = re.split(r"<-+>", sent)
				collect_splits = []
				for split in split_utt:
					collect_splits = collect_splits + re.split(r"<sil[^>]+>", split)			
				for u in collect_splits:
					tokenized_segment = self.tokenize_finegrained(u, tokenized_segment, spkr)
			else:
				tokenized_segment = self.tokenize_finegrained(sent, tokenized_segment, spkr)
		return tokenized_segment


	def tokenize_finegrained(self, utt, tokenized_segment, main_speaker):
		if "<" not in utt:
			tokenized_segment.append(utt.strip(" ") + "\t" + main_speaker)
			return tokenized_segment
		index = 0
		while index < len(utt):
			if utt[index] == "<" and index+6 < len(utt) and "quest:" in utt[index:index+7]:
				index += 6
				question, index = self.get_question(utt, index)
				if len(question.strip()):
					tokenized_segment.append(question +"\t" + main_speaker)
			elif utt[index] == "<":
				interject_spkr, interject_utt, index = self.get_interjected_utt(utt, index)
				if len(interject_utt):
					tokenized_segment.append(interject_utt + "\t" + interject_spkr)
			else:
				subut, index = self.get_sub_utt(utt, index)
				#print(subut, index, main_speaker)
				if len(subut):
					tokenized_segment.append(subut + "\t" + main_speaker)
		return tokenized_segment

	def get_question(self,utt, index):
		question = ""
		while utt[index] not in ["/",">"]:
			if index+2 < len(utt) and utt[index:index+3]== "/>":
				question += utt[index]
				index+=2
				return question.strip(), index
			question += utt[index]
			index+=1
		return question.strip(), index

	def get_sub_utt(self, utt, index):
		sub_u = ""
		while index < len(utt) and utt[index] != "<":
			sub_u+= utt[index]
			index += 1
		#print(sub_u, index, utt[index-1])
		return sub_u.strip(" "), index


	def get_interjected_utt(self, utt, index):
		interject_tag = ""
		#print("UTT: ", utt, index)
		while utt[index]!= ">":
			#print(utt, interject_tag)
			interject_tag+= utt[index]
			index += 1
		#print(utt, interject_tag)
		spkr = interject_tag.split(" ")[1].strip(",")
		interject_utt = " ".join(interject_tag.split(" ")[2:]).strip(">")
		if spkr == "i":
			spkr = "spkr1"
		else:
			spkr = "spkr2"
		#print(spkr, interject_utt, index)
		return spkr, interject_utt.strip(" "), index


###############################################
	def third_pass(self, lines):
		out_lines = self.split_false_starts(lines)
		out_lines = self.split_neg_aff(out_lines)
		out_lines = self.split_backchannel(out_lines)
		return out_lines

	def delete_empty_utts(self,lines):
		out_lines = []
		for line in lines:
			if line.split("\t")[0] in [""," ", "  ", "?", " ?", None]:
				continue
			out_lines.append(line.strip())
		return out_lines

	def split_false_starts(self, lines):
		out_lines = []
		for line in lines:
			utt, spkr = line.split("\t")[0], line.split("\t")[1]
			if "/" in utt:
				for u in utt.split("/"):
					if u == "" or u is None:
						continue
					#print(u +"\t"+spkr)
					if len(u):
						out_lines.append(u +"\t"+spkr)
			else:
				out_lines.append(line)
		return out_lines

	def split_neg_aff(self,lines):
		NEGAFF = re.compile(r"^nein |^ja,?? |^ja ja,?? |^nee,?? |^nö,?", re.IGNORECASE)
		out_lines = []
		for line in lines:
			sent, spkr = line.split("\t")[0], line.split("\t")[1]
			match = re.match(NEGAFF, sent)
			if match:	
				negaff, rest = sent[:match.span()[1]],sent[match.span()[1]:]
				out_lines.append(negaff +"\t"+spkr)
				if len(rest):
					out_lines.append(rest +"\t"+spkr)
			else:
				out_lines.append(line)
		return out_lines

	def split_backchannel(self,lines):
		BACKCHANNEL = re.compile(r"ok |okay |alles klar |^gut |^super |ok super", re.IGNORECASE)
		out_lines = []
		for line in lines:
			sent, spkr = line.split("\t")[0], line.split("\t")[1]
			match = re.match(BACKCHANNEL, sent)
			if match:
				bckchnl, rest = sent[:match.span()[1]],sent[match.span()[1]:]
				out_lines.append(bckchnl +"\t"+spkr)
				if len(rest):
					out_lines.append(rest +"\t"+spkr)
			else:
				out_lines.append(line)
		return out_lines


###########################################################
	def fourth_pass(self, lines):
		out_lines = self.split_non_embedded_questions(lines)
		out_lines = self.split_at_und_dann(out_lines)
		return out_lines

	def split_non_embedded_questions(self,lines):
		out_lines = []
		for line in lines:
			sent, spkr = line.split("\t")[0], line.split("\t")[1]
			s = self.tagger(sent)
			taglist = [t.tag_ for t in s]
			if "ROOT" in taglist and "PWAV" in taglist:
				out_lines.append(line)
				continue
			elif "PWAV" in taglist:
				i = 0
				while taglist[i] != "PWAV":
					i+=1
				first, question = " ".join([t.text for t in s[:i]]), " ".join([t.text for t in s[i:]])
				out_lines.append(first +"\t" +spkr)
				out_lines.append(question +"\t" +spkr)
			else:
				out_lines.append(line)
		return out_lines

	def split_at_und_dann(self, lines):
		UNDDANN = re.compile(r"und (dann|jetzt) ")
		out_lines = []
		for line in lines:
			line = re.sub(r" +" , " ", line)
			sent, spkr = line.split("\t")[0], line.split("\t")[1]
			match = re.search(UNDDANN, sent)
			if match and match.span()[0] > 0:
				first, unddann = sent[:match.span()[0]], sent[match.span()[0]:]
				out_lines.append(first +"\t"+spkr)
				out_lines.append(unddann +"\t"+spkr)
			else:
				out_lines.append(line)
		return out_lines

	def remove_punct(self,lines):
		PUNCT = re.compile(r"[\.,\:\;\?\!><\*]")
		out_lines = []
		for line in lines:
			line = re.sub(PUNCT, "", line)
			out_lines.append(line.strip(" "))
		return out_lines


	def parse(self, lines):
		lines = self.first_pass(lines)
		lines = self.second_pass(lines)
		lines = self.third_pass(lines)
		lines = self.fourth_pass(lines)
		lines = self.delete_empty_utts(lines)
		lines = self.remove_punct(lines)
		lines = self.delete_empty_utts(lines)
		return lines



if __name__ == "__main__":
	import os 

	def create_data_list(path, postfix):
		data = []
		for dirpath, dirnames, filenames  in os.walk(path):
			for filename in filenames:
				if not filename.endswith(postfix):
					continue
				data.append(os.path.join(dirpath,filename))
		return data

	p = DialogParser()
	if sys.argv[1] != "test":
		data = create_data_list(sys.argv[1], ".txt")
		for d in data:
			with open (d) as infile:
				lines = p.parse(infile)
				filename = d.split("/")[-1].strip(".txt")
				path = "/".join(d.split("/")[:-2])+"/parsed_dialogs/"+ filename+ "-parsed.tsv"
				with open(path, "w+") as otufile:
					for line in lines:
						otufile.write(line+"\n")
	else:
		lines = p.parse(['05I005	ja ist egal. <-> und welche <noise> <quest: an un/ (ei)ne> </noise: klappern> Fl"ugelschraube <-> nee, zwei Fl"ugelschrauben.'])
		print("LINEBEFORE: ",'05I005	ja ist egal. <-> und welche <noise> <quest: an un/ (ei)ne> </noise: klappern> Fl"ugelschraube <-> nee, zwei Fl"ugelschrauben.')
		for line in lines:
				print(line)
		lines = p.parse(['05K005	{<sil: 2>}<hum: atmen> Fl"ugelschrauben, was <par> ist das? </par: 2>'])
		print("LINEBEFORE: ",'05K005	{<sil: 2>}<hum: atmen> Fl"ugelschrauben, was <par> ist das? </par: 2>')
		for line in lines:
				print(line)
		lines = p.parse(['05I021	<sil: 5> <hum: lachen> <hum: atmen> {"ahm}<attrib: z"ogern> <sil: 8> <hum: atmen> {jetzt}<attrib: z"ogern> f"angst du auf der anderen Seite an also "ah auf der entgegengesetzten Seite vom W"urfel. <hum: atmen> {und}<attrib: z"ogern> <noise> drehst da eine </noise: klappern> <sil: 2> <hum: schmatzen> <sil: 1> legst oben drauf diese beiden langen Teile <noise> wieder senkrecht so zum Fl"ugel. <--> und zwar {"ahm}<attrib: z"ogern> </noise: rascheln> ab dem zweiten Loch. <sil: 1> verstehst du, also nicht das erste Loch blei/ {l"a"st du frei}<spk: K, mhm, mhm> <-> und ab dem zweiten legst du die beiden <--> <noise> {und "ah}<attrib: z"ogern> drehst die fest mit einmal </noise: rascheln> <sil: 1> "ahm mit der orangenen <noise> Achtkantschraube und mit der orangenen <-> anderen Schraube. </noise: rascheln>'])
		print("LINEBEFORE: ",'05I021	<sil: 5> <hum: lachen> <hum: atmen> {"ahm}<attrib: z"ogern> <sil: 8> <hum: atmen> {jetzt}<attrib: z"ogern> f"angst du auf der anderen Seite an also "ah auf der entgegengesetzten Seite vom W"urfel. <hum: atmen> {und}<attrib: z"ogern> <noise> drehst da eine </noise: klappern> <sil: 2> <hum: schmatzen> <sil: 1> legst oben drauf diese beiden langen Teile <noise> wieder senkrecht so zum Fl"ugel. <--> und zwar {"ahm}<attrib: z"ogern> </noise: rascheln> ab dem zweiten Loch. <sil: 1> verstehst du, also nicht das erste Loch blei/ {l"a"st du frei}<spk: K, mhm, mhm> <-> und ab dem zweiten legst du die beiden <--> <noise> {und "ah}<attrib: z"ogern> drehst die fest mit einmal </noise: rascheln> <sil: 1> "ahm mit der orangenen <noise> Achtkantschraube und mit der orangenen <-> anderen Schraube. </noise: rascheln>')
		for line in lines:
				print(line)
		lines = p.parse(['05I009	<noise> oben setzt du {"ahm}<attrib: z"ogern> (ei)ne gelbe Schraube "ahm <-> (ei)ne runde mit Schlitz ein, <-> also einfach reinstecken. <-> und dahinter die mit {den}<attrib: z"ogern> acht Ecken, <-> acht Kanten. <sil: 1> <hum: schmatzen> </noise: rascheln> dann nimmst {du "ahm}<attrib: z"ogern> dies Sch/ "ah mit den drei L"ochern das St"uck H"olzchen <hum: atmen> und legst das so unter das es halt unter die beiden Schrauben <-> pa"st von unten. <noise> <sil: 2> hast du? </noise: rascheln>'])
		print("LINEBEFORE: ",'05I009	<noise> oben setzt du {"ahm}<attrib: z"ogern> (ei)ne gelbe Schraube "ahm <-> (ei)ne runde mit Schlitz ein, <-> also einfach reinstecken. <-> und dahinter die mit {den}<attrib: z"ogern> acht Ecken, <-> acht Kanten. <sil: 1> <hum: schmatzen> </noise: rascheln> dann nimmst {du "ahm}<attrib: z"ogern> dies Sch/ "ah mit den drei L"ochern das St"uck H"olzchen <hum: atmen> und legst das so unter das es halt unter die beiden Schrauben <-> pa"st von unten. <noise> <sil: 2> hast du? </noise: rascheln>')
		for line in lines:
				print(line)
		lines = p.parse(['02I048	<noise> {<sil: 3>}<hum: atmen> so jetzt sieht(_e)s *autz <-> aus wie vorher. </noise: rascheln>'])
		print("LINEBEFORE: ",'02I048	<noise> {<sil: 3>}<hum: atmen> so jetzt sieht(_e)s *autz <-> aus wie vorher. </noise: rascheln>')
		for line in lines:
				print(line)
		lines = p.parse(['02I048	<noise> auch dadrunter <sil: 1> <hum: lachen> <sil: 3> das wird jetzt das der Bug. </noise: rascheln> <sil: 2> {<quest: ?>}<spk: K, mhm> {so.}<noise: rascheln>'])
		print("LINEBEFORE: ",'02I048	<noise> auch dadrunter <sil: 1> <hum: lachen> <sil: 3> das wird jetzt das der Bug. </noise: rascheln> <sil: 2> {<quest: ?>}<spk: K, mhm> {so.}<noise: rascheln>')
		for line in lines:
				print(line)