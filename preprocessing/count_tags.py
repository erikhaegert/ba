import sys

import sys 
import re
import json 
import os
from collections import Counter

def create_data_list(path, postfix):
	data = []
	for dirpath, dirnames, filenames  in os.walk(path):
		for filename in filenames:
			if not filename.endswith(postfix):
				continue
			data.append(os.path.join(dirpath,filename))
	return data
data = create_data_list(sys.argv[1], ".tsv")
sc = Counter()
ac = Counter()
UU = 0
DU = 0
for d in data:
	with open(d) as infile:
		for line in infile:
			if line.startswith("#"):
				continue
			#print(line)
			state = line.split("\t")[2]
			act= line.split("\t")[3].strip()
		
			sc[state] +=1
			ac[act]+=1
			UU +=1
			if act == "InitI":
				DU +=1
sc["7"]= DU
for k in sc.keys():
	sc[k] = (sc[k]/(UU+DU))* 100
for k in ac.keys():
	ac[k] = (ac[k]/UU) * 100
print(json.dumps(sc, indent=4))
print(json.dumps(ac, indent=4))