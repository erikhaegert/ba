import sys
with open(sys.argv[1]) as infile:
	for line in infile:
		utt, rest  = line.split("\t")[0], line.split("\t")[1:]
		line = [utt.lower()] +rest
		line = "\t".join(line)
		print(line)