import sys 
import re
import json 
import os
def create_data_list(path, postfix):
	data = []
	for dirpath, dirnames, filenames  in os.walk(path):
		for filename in filenames:
			if not filename.endswith(postfix):
				continue
			data.append(os.path.join(dirpath,filename))
	return data


def is_bad_format(line):
	#print(line.split("\t"))
	if line.startswith("#"):
		return False
	if len(line.split("\t")) != 4:
		if len(line.split("\t")) == 5 and line.split("\t")[-1].startswith("#"):
			return False
		return True
	return False

def tag_doesnt_exist(line):
	if line.startswith('#'):
		return False
	segments = line.split("\t")
	if len(segments)< 4:
		return False
	if segments[3] in ["InitI", "RepI","RepR", "ReqRepI", "ReqRepR", "ReqAckI", "ReqAckR", "CanI", "CanR","AckI", "AckR","ContI", "ContR"]:
		return False
	if segments[2]  in ["1","2","3","4","5","6"]:
		return False
	return True

def role_doesnt_match(role, act):
	#print(role, act)
	#role muss "I" oder "R" sein!
	if act.strip("\n")[-1] == role:
		return False
	return True




def double_init(line1, line2):
	spkr1, act1 = line1.split("\t")[1].strip(), line1.split("\t")[3].strip()
	spkr2, act2 = line2.split("\t")[1].strip(), line2.split("\t")[3].strip()
	if act1 == "InitI" or act2 == "InitI":
		if spkr1 == spkr2 and act1 == act2:
			return True
		return False
	return False

def report_error(file, line, etype, report):
	if report[file].get(line+1,0) == 0:
		report[file][line+1]={}
		report[file][line+1][etype]=True
	else:
		report[file][line+1][etype]=True
	return report

path = sys.argv[1]
data = create_data_list(path, ".tsv")
report = {}
initiator = ""
for d in data:
	report[d.split("/")[-1]]={}
	with open(d) as infile:
		lines = infile.readlines()
		for i in range(len(lines)):
			if lines[i].startswith("#"):
				continue
			if is_bad_format(lines[i]):
				report = report_error(d.split("/")[-1],i,"bad_format", report)
			if tag_doesnt_exist(lines[i]):
				report = report_error(d.split("/")[-1],i,"bad_tag", report)
			if 0 < len(lines[i].split("\t")) > 3:
				if lines[i].split("\t")[3].strip() == "InitI":
					initiator = lines[i].split("\t")[1]
				if lines[i].split("\t")[1] == initiator and role_doesnt_match("I",lines[i].split("\t")[3]):
					report = report_error(d.split("/")[-1],i,"role_mismatch", report)
				if lines[i].split("\t")[1] != initiator and  role_doesnt_match("R",lines[i].split("\t")[3]):
					#print(lines[i].split("\t")[1], initiator)
					report = report_error(d.split("/")[-1],i,"role_mismatch", report)
		#for i in range(len(lines)-1):
		#	if len(lines[i].split("\t")) < 4 or len(lines[i+1].split("\t")) < 4:
		#			continue
		#	if double_init(lines[i], lines[i+1]):	
		#		report = report_error(d.split("/")[-1],i+1,"double_init", report)
print(json.dumps(report, indent=4))