import sys
with open(sys.argv[1]) as infile:
	with open(sys.argv[2], "a") as outfile:
		line_c = 1
		for line in infile:
			if len(line.split("\t")) != 2:
				print(line, line_c)
				#line_c +=1
				#continue
			sent, rest = line.split("\t")[0], line.split("\t")[1]
			if len(rest.split()) != 3:
				print("SECOND: ", line, line_c)
				#line_c +=1
				#continue
			rest = "\t".join(rest.split())
			out_line = "\t".join([sent,rest])
			outfile.write(out_line+"\n")
			line_c +=1

